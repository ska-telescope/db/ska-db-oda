ARG BUILD_IMAGE="artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.12.0"
ARG BASE_IMAGE="artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.12.0"

FROM $BUILD_IMAGE AS buildenv
FROM $BASE_IMAGE

ENV APP_USER="oda"
ENV APP_DIR="/app"

ARG CAR_PYPI_REPOSITORY_URL=https://artefact.skao.int/repository/pypi-internal
ENV PIP_INDEX_URL ${CAR_PYPI_REPOSITORY_URL}/simple

USER root

RUN adduser $APP_USER --disabled-password --home $APP_DIR

WORKDIR $APP_DIR

COPY --chown=$APP_USER:$APP_USER . .

RUN poetry config virtualenvs.create false
# Install runtime dependencies and the app
RUN poetry install --no-root

# We want a scratch directory for filesystem backend
RUN mkdir -p /var/lib/oda && chown -R ${APP_USER} /var/lib/oda

USER ${APP_USER}

CMD ["fastapi", \
    "run", \
    "src/ska_db_oda/rest/app.py", \
    # Trust TLS headers set by nginx ingress:
    "--proxy-headers", \
    "--port", "5000" \
]
