.. _oso_context:

OSO Context
=============

OSO consists of several applications within a Shared Data Architecture. Each application is responsible for a different stage in the observation preparation workflow,
producing data entities that need to be persisted and reading data entities from previous stages.

The ODA is the central shared database in this architecture. The applications do not communicate with each other, instead they read and write entities to the ODA. The ODA
therefore supports all stages, provides a record of what happened at each stage and ultimately allows the science data products created by the SKAO to be linked back to the
astronomer.

.. figure:: ../diagrams/oso-workflow.jpg
   :align: center



