Repository and UnitOfWork
==========================

The ODA provides a UnitOfWork and Repository abstraction which should be the main interface to the ODA
from within the OSO Python applications. The diagram below shows the expected architecture of OSO, and which applications
will use the Repository interface.

.. figure:: ../../diagrams/oso-deployment.jpg
   :align: center

The Repository and UnitOfWork are abstract classes, and the method of storage used depends on which implementation of the interfaces are used in the client application: ``filesystem`` or ``postgres``.

The following environment variables are used to configure the Repository and UnitOfWork. For more details on setting these values inside the via the Helm chart
for the ``ska-db-oda`` server, see :doc:`../../deployment/deployment_to_kubernetes`. A similar process should be in place for any application which imports the classes, e.g. ``ska-oso-services``.

.. list-table:: Environment variables used by Repository and UnitOfWork implementations
   :widths: 20 10 50 20
   :header-rows: 1

   * - Environment variable
     - Relevant implementation
     - Description
     - Required/optional
   * - SKUID_URL
     - All
     - The address of a running SKUID instance that IDs can be fetched from.
     - ``http://ska-ser-skuid-test-svc:9870``
   * - POSTGRES_HOST
     - ``postgres``
     - The address of the PostgreSQL instance to connect to, e.g. ``12.34.56.7``
     - Required
   * - POSTGRES_PORT
     - ``postgres``
     - The port of the PostgreSQL instance to connect to, e.g. ``5432``
     - Optional - default: ``5432``
   * - POSTGRES_DB_NAME
     - ``postgres``
     - The name of the database in the PostgreSQL instance, e.g. ``postgres``
     - Optional - default: ``postgres``
   * - ADMIN_POSTGRES_USER
     - ``postgres``
     - The admin user in the PostgreSQL instance, e.g. ``postgres``
     - Optional - default: ``postgres``
   * - ADMIN_POSTGRES_PASSWORD
     - ``postgres``
     - The password for the admin user in the PostgreSQL instance.
     - Required
   * - ODA_DATA_DIR
     - ``filesystem``
     - The base directly the ODA will store entities, in entity specific sub-directories. Ensure the application can write to this directory.
     - ``/var/lib/oda``


.. note::
   This is a work in progress as is expected to be completed as part of BTN-2414


.. toctree::
    :maxdepth: 1
    :caption: Repository

    python_docs/AbstractRepository
    python_docs/PostgresRepository
    python_docs/FileSystemRepository


.. toctree::
    :maxdepth: 1
    :caption: UnitOfWork

    python_docs/AbstractUnitOfWork
    python_docs/PostgresUnitOfWork
    python_docs/FilesystemUnitOfWork