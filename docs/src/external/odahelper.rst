.. _odahelper:

ODA Helper Module
==================

The ODA Helper module offers simple functions which interact with the API, allowing users not to worry about building and
handling HTTP requests. So far, it offers functions for specific use cases, rather than acting as a general client library.

As use cases for the ODA expand, we may generalise this helper module. For now we are happy with a lightweight module that
is simple to change and provides the required functionality.

Execution Blocks
------------------
For example, the ODA offers a custom API for Execution Blocks, so they can be created and updated during telescope
operation without operators needing to know the internal ODA details. See the :ref:`rest_api` documentation for more.

Generally, it is expected that the ``create_eb`` function will be called at the start of a session, and function calls
where the request/response should be stored in the ExecutionBlock should be decorated with ``@capture_request_response``.
For more context on Execution Blocks, see the `section in the Scripting documentation <https://developer.skao.int/projects/ska-oso-scripting/en/latest/execution_blocks.html>`_

To view the ExecutionBlock in the ODA, the ODA CLI can be used perform queries (see :ref:`CLI`). Alternatively, the ODA REST API could be used directly.

Module Functions
-----------------

.. automodule:: ska_db_oda.client.oda_helper
   :members: