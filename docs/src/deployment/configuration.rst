.. _configuration:

Configuration
==============

The following environment variables are used to configure the application. They are set via a Kubernetes ConfigMap
with values coming from the Helm values.yaml. See :doc:`deployment_to_kubernetes`

Generally, if an variable default can be set in the application it will be. Some 'defaults' are more dynamic
and need release or namespace info. In this case a sensible default has been set in the charts.

.. list-table:: Environment variables used by ska-db-oda
   :widths: 10 10 10 10 10
   :header-rows: 1

   * - Environment variable
     - Description
     - Required/optional in the application
     - Corresponding Helm value
     - Required/optional in the Helm chart
   * - SKUID_URL
     - The Kubernetes service address of a running SKUID service
     - Required
     - ``ska-db-oda.rest.skuid.url``
     - Optional - will fall back on: ``ska-ser-skuid-{{ .Release.Name }}-svc.{{ .Release.Namespace }}.svc.{{ .Values.global.cluster_domain }}:9870``
   * - ODA_BACKEND_TYPE
     - Defines whether the ODA interfaces should connect to a Postgresql instance or use the filesystem.
     - Optional - default: ``postgres``
     - ``ska-db-oda.rest.backend.type``
     - Required - default set to ``postgres``
   * - ODA_DATA_DIR
     - The base filesystem location that the filesystem ODA will use to store and retrieve entities.
     - Optional - default: ``/var/lib/oda``
     - ``ska-db-oda.rest.backend.filesystem.pv_mountpoint``
     - Required - default set to ``/var/lib/oda``
   * - POSTGRES_HOST
     - The address of the PostgreSQL instance that the postgres ODA will connect to.
     - Required if ``ODA_BACKEND_TYPE`` is ``postgres``
     - ``ska-db-oda.rest.backend.postgres.host``
     - Optional - will fall back on: ``{{ .Release.Name }}-postgresql.{{ .Release.Namespace }}.svc.{{ .Values.global.cluster_domain }}``
   * - ADMIN_POSTGRES_USER
     - The admin user of the PostgreSQL instance that the postgres ODA will connect to.
     - Optional - default: ``postgres``
     - ``ska-db-oda.rest.backend.postgres.user``
     - Optional - no default in chart
   * - ADMIN_POSTGRES_PASSWORD
     - The admin password of the PostgreSQL instance that the postgres ODA will connect to.
     - Required if ``ODA_BACKEND_TYPE`` is ``postgres``
     - Pulled from Vault - see :doc:`secret_management`
     -
   * - POSTGRES_PORT
     - The port of the PostgreSQL instance that the postgres ODA will connect to.
     - Optional - default: ``5432``
     - ``ska-db-oda.rest.backend.postgres.port``
     - Optional - no default in chart
   * - POSTGRES_DB_NAME
     - The name of the database within a PostgreSQL instance that the postgres ODA will connect to.
     - Optional - default: ``postgres``
     - ``ska-db-oda.rest.backend.postgres.db.name``
     - Optional - no default in chart