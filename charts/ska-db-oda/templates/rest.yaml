{{ if .Values.rest.enabled }}
{{ if .Values.rest.ingress.enabled }}
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ template "ska-db-oda.name" . }}-{{ .Values.rest.component }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "ska-db-oda.labels" . | indent 4 }}
    component: {{ .Values.rest.component }}
    function: {{ .Values.rest.function }}
    domain: {{ .Values.rest.domain }}
    intent: production
spec:
  ingressClassName: nginx
  rules:
    - http:
        paths:
          - path: /{{ .Release.Namespace }}/oda/api
            pathType: Prefix
            backend:
              service:
                name: {{ template "ska-db-oda.name" . }}-{{ .Values.rest.component }}-{{ .Release.Name }}
                port:
                  number: 5000
{{ end }}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ template "ska-db-oda.name" . }}-{{ .Values.rest.component }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "ska-db-oda.labels" . | indent 4 }}
    component: {{ .Values.rest.component }}
    function: {{ .Values.rest.function }}
    domain: {{ .Values.rest.domain }}
    intent: production
spec:
  selector:
    app: {{ template "ska-db-oda.name" . }}
    component: {{ .Values.rest.component }}
  ports:
  - protocol: TCP
    port: 5000
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ template "ska-db-oda.name" . }}-{{ .Values.rest.component }}-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "ska-db-oda.labels" . | indent 4 }}
    component: {{ .Values.rest.component }}
    function: {{ .Values.rest.function }}
    domain: {{ .Values.rest.domain }}
    intent: production
spec:
  selector:
    matchLabels:
      app: {{ template "ska-db-oda.name" . }}
      component: {{ .Values.rest.component }}
  serviceName: rest-{{ template "ska-db-oda.name" . }}-{{ .Release.Name }}
  replicas: 1
  template:
    metadata:
      labels:
        {{- include "ska-db-oda.labels" . | indent 8 }}
        component: {{ .Values.rest.component }}
        function: {{ .Values.rest.function }}
        domain: {{ .Values.rest.domain }}
        intent: production
    spec:
      {{ if .Values.rest.backend.filesystem.use_pv }}
      {{ if .Values.global.minikube }}
{{/* Must run as root inside Pod to write to root-owned Minikube PV directory. See https://github.com/kubernetes/minikube/issues/1990 */}}
      securityContext:
        runAsUser: 0
        runAsGroup: 0
      {{ end }}
      {{ end }}
      volumes:
      {{ if .Values.rest.backend.filesystem.use_pv }}
        - name: data
          persistentVolumeClaim:
            claimName: {{ template "ska-db-oda.name" . }}-persistent-volume-claim-{{ .Release.Name }}
      {{ end }}
      containers:
      - name: oda-rest
        image: "{{ .Values.rest.image.registry }}/{{ .Values.rest.image.image }}:{{$.Values.rest.image.tag | default $.Chart.AppVersion}}"
        imagePullPolicy: {{ .Values.rest.image.pullPolicy }}
        # Command and args default to the Dockerfile values
        env:
          - name: ADMIN_POSTGRES_PASSWORD
            valueFrom:
              secretKeyRef:
                {{ if $.Values.rest.backend.postgres.password.secret }}
                name: {{ $.Values.rest.backend.postgres.password.secret }}
                key: {{ $.Values.rest.backend.postgres.password.key }}
                {{ else }}
                name: ska-db-oda-umbrella-postgres-{{ $.Release.Name }}
                key: ADMIN_POSTGRES_PASSWORD
                {{- end }}
        envFrom:
          - configMapRef:
              name: {{ template "ska-db-oda.name" . }}-environment-{{ .Release.Name }}
        ports:
          - name: oda-rest
            containerPort: 5000
        volumeMounts:
          {{ if .Values.rest.backend.filesystem.use_pv }}
          - name: data
            mountPath: {{.Values.rest.backend.filesystem.pv_mountpoint }}
          {{ end }}
        resources:
{{ toYaml .Values.rest.resources | indent 10 }}
  {{- with .Values.nodeSelector }}
nodeSelector:
  {{ toYaml . | indent 8 }}
  {{- end }}
  {{- with .Values.affinity }}
affinity:
  {{ toYaml . | indent 8 }}
  {{- end }}
  {{- with .Values.tolerations }}
tolerations:
  {{ toYaml . | indent 8 }}
  {{- end }}
  {{ end }}
