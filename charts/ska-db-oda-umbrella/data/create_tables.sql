--Postgres Table DDL scripts for 4 new entities to be introduced as a part of PI16
--Currently these tables have an ID as the primary key, info column of jsonb datatype and few additional columns split from the metadata
--The new entities are SBI, Execution Blocks , Projects and Observing Programs

--Status Enumerator with predefined values
CREATE TYPE sbd_status as ENUM('Draft', 'Submitted', 'Ready', 'In Progress', 'Observed', 'Suspended', 'Failed Processing', 'Complete');
CREATE TYPE sbi_status as ENUM('Created', 'Executing', 'Observed', 'Failed');
CREATE TYPE eb_status as ENUM('Created', 'Fully Observed', 'Failed');
CREATE TYPE prj_status as ENUM('Draft', 'Submitted', 'Ready', 'In Progress', 'Observed', 'Complete', 'Cancelled', 'Out of Time');

--Create table script for new entities

-- Placeholder for SBDs
CREATE TABLE tab_oda_sbd
(id serial NOT NULL PRIMARY KEY,
info jsonb NOT NULL,
sbd_id text NOT NULL,
version integer NOT NULL,
created_by text NOT NULL,
created_on timestamp with time zone NOT NULL,
last_modified_on timestamp with time zone NOT NULL,
last_modified_by text NOT NULL);

-- Table for SBD History
CREATE TABLE tab_oda_sbd_status_history
 (id serial NOT NULL,
 sbd_ref text NOT NULL,
 sbd_version integer NOT NULL,
 previous_status sbd_status NOT NULL,
 current_status sbd_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, sbd_ref, sbd_version, current_status));
-- Keeping the flexibility for storing multiple status with same value
-- https://skao.slack.com/archives/C06N7M53SSG/p1714464389367289 https://skao.slack.com/archives/C06N7M53SSG/p1714465049324699

--Table for SBIS
CREATE TABLE tab_oda_sbi
 (id serial NOT NULL PRIMARY KEY,
 sbd_id text NOT NULL,
 sbd_version serial NOT NULL,
 info jsonb NOT NULL,
 sbi_id text NOT NULL,
 version integer default 1,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL
);

-- Table for SBI History
CREATE TABLE tab_oda_sbi_status_history
 (id serial NOT NULL,
 sbi_ref text NOT NULL,
 sbi_version integer NOT NULL,
 previous_status sbi_status NOT NULL,
 current_status sbi_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, sbi_ref, sbi_version, current_status));

-- Table for Execution Blocks
CREATE TABLE tab_oda_eb
 (id serial NOT NULL PRIMARY KEY,
 sbd_id text ,
 sbd_version integer ,
 eb_id text NOT NULL,
 sbi_id text,
 info jsonb NOT NULL,
 version integer default 1,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL );

-- Table for EB History
 CREATE TABLE tab_oda_eb_status_history
 (id serial NOT NULL,
 eb_ref text NOT NULL,
 eb_version integer NOT NULL,
 previous_status eb_status NOT NULL,
 current_status eb_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, eb_ref, eb_version, current_status));

--Table for Projects
CREATE TABLE tab_oda_prj
 (id serial NOT NULL PRIMARY KEY,
 prj_id text NOT NULL,
 info jsonb NOT NULL,
 version integer default 1,
 author text NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL );

 -- Table for Project History
 CREATE TABLE tab_oda_prj_status_history
 (id serial NOT NULL,
 prj_ref text NOT NULL,
 prj_version integer NOT NULL,
 previous_status prj_status NOT NULL,
 current_status prj_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, prj_ref, prj_version, current_status));

--Table for Observing Programs
CREATE TABLE tab_oda_obs_prg
 (id serial NOT NULL PRIMARY KEY,
 obs_prg_id text NOT NULL,
 info jsonb NOT NULL,
 version integer default 1,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL );


--Table for Proposal
CREATE TABLE tab_oda_prsl
(
    id serial NOT NULL PRIMARY KEY,
    prsl_id text NOT NULL,
    submitted_by text,
    submitted_on text,
    status text,
    cycle text,
    investigators text[],
    info jsonb NOT NULL,
    version integer default 1,
    created_by text NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified_on timestamp with time zone NOT NULL,
    last_modified_by text NOT NULL
);


--Table for Shift Log Tool
CREATE TABLE public.tab_oda_slt (
    id SERIAL PRIMARY KEY,
    shift_id VARCHAR(50) NOT NULL,
    shift_start TIMESTAMPTZ NOT NULL,
    shift_end TIMESTAMPTZ,
    shift_operator VARCHAR(100) NOT NULL,
    shift_logs JSONB,
    annotations TEXT,
    created_by VARCHAR(50) NOT NULL,
    created_on TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_modified_by VARCHAR(50) NOT NULL,
    last_modified_on TIMESTAMPTZ NOT NULL,
    CONSTRAINT unique_shift UNIQUE (shift_id, shift_start),
    CONSTRAINT unique_shift_id UNIQUE (shift_id)
);

CREATE INDEX idx_tab_oda_slt_shift_id ON public.tab_oda_slt (shift_id);
CREATE INDEX idx_tab_oda_slt_shift_start ON public.tab_oda_slt (shift_start);


--Table for Shift Comments
CREATE TABLE public.tab_oda_slt_shift_comments (
    id SERIAL PRIMARY KEY,
    shift_id VARCHAR(50) NOT NULL,
    operator_name VARCHAR(100) NOT NULL,
    comment TEXT,
    image jsonb NULL,
    created_by VARCHAR(100) NOT NULL,
    created_on TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_modified_on TIMESTAMPTZ NOT NULL,
    last_modified_by VARCHAR(100) NOT NULL,
    CONSTRAINT fk_shift FOREIGN KEY (shift_id) REFERENCES public.tab_oda_slt(shift_id)
);

CREATE INDEX idx_tab_oda_slt_shift_comments_shift_id ON public.tab_oda_slt_shift_comments (shift_id);
CREATE INDEX idx_tab_oda_slt_shift_comments_operator_name  ON public.tab_oda_slt_shift_comments (operator_name);


--Table for Shift Log Comments
CREATE TABLE public.tab_oda_slt_shift_log_comments (
    id SERIAL PRIMARY KEY,
    shift_id VARCHAR(50) NOT NULL,
    eb_id VARCHAR(60) NOT NULL,
    operator_name VARCHAR(100) NOT NULL,
    log_comment TEXT,
    image jsonb NULL,
    created_by VARCHAR(100) NOT NULL,
    created_on TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_modified_by VARCHAR(100) NOT NULL,
    last_modified_on TIMESTAMPTZ NOT NULL,    
    CONSTRAINT fk_shift FOREIGN KEY (shift_id) REFERENCES public.tab_oda_slt(shift_id)
);

CREATE INDEX idx_tab_oda_slt_shift_log_comments_shift_id ON public.tab_oda_slt_shift_log_comments (shift_id);
CREATE INDEX idx_tab_oda_slt_shift_log_comments_eb_id ON public.tab_oda_slt_shift_log_comments (eb_id);
CREATE INDEX idx_tab_oda_slt_shift_log_comments_operator_name ON public.tab_oda_slt_shift_log_comments (operator_name);


--Table for Shift Annotations
CREATE TABLE public.tab_oda_slt_shift_annotations (
    id SERIAL PRIMARY KEY,
    shift_id VARCHAR(50) NOT NULL,
    user_name VARCHAR(100) NOT NULL,
    annotation TEXT,
    created_by VARCHAR(100) NOT NULL,
    created_on TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    last_modified_on TIMESTAMPTZ NOT NULL,
    last_modified_by VARCHAR(100) NOT NULL,
    CONSTRAINT fk_shift FOREIGN KEY (shift_id) REFERENCES public.tab_oda_slt(shift_id)
);

CREATE INDEX idx_tab_oda_slt_shift_annotations_shift_id ON public.tab_oda_slt_shift_annotations (shift_id);
CREATE INDEX idx_tab_oda_slt_shift_annotations_user_name  ON public.tab_oda_slt_shift_annotations (user_name);