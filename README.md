# SKA Database OSO Data Archive

The repository for the SKA’s ‘Observatory Science Operations’ (OSO) Data Archive

## Project Description
This project contains code for the Data Archive and accompanying client library. The Data Archive is a prototype JSON document store for the SKA’s ‘Observatory Science Operations’ (OSO) subsystem.
The project contains clients for three implementations of the ODA: using memory, filesystem or PostgreSQL for persistence. The Helm chart will also deploy an instance of PostgreSQL.

## Quickstart


To clone this repository, run

```
git clone --recurse-submodules git@gitlab.com:ska-telescope/db/ska-db-oda.git
```

To refresh the GitLab Submodule, execute below commands:

```
git submodule update --recursive --remote
git submodule update --init --recursive
```

## Build and test

Install dependencies with Poetry and activate the virtual environment

```
poetry install
poetry shell
```

To build a new Docker image for the ODA, run

```
make oci-build
```

Execute the test suite and lint the project with:

```
make python-test
make python-lint
```

To run a helm chart unit tests to verify helm chart configuration:
 
```
helm plugin install https://github.com/helm-unittest/helm-unittest.git
make k8s-chart-test
```

## Deploy to Kubernetes

Install the umbrella Helm chart, to deploy the ODA REST Client (using postgres backend), Postgres and pgadmin:

```
make k8s-install-chart
```

The Swagger UI should be available external to the cluster at `http://<KUBE_HOST>/<KUBE_NAMESPACE>/oda/api/v<MAJOR_VERSION>/ui/` and the API accesible via the same URL.

If using minikube, `KUBE_HOST` can be found by running `minikube ip`. 
`KUBE_NAMESPACE` is the namespace the chart was deployed to, likely `ska-db-oda`

The PostgreSQL instance should also be accessible (when postgres is enabled in the Helm values). The IP of the LoadBalancer can be found with
``kubectl get svc -n ska-db-oda | grep postgresql | grep LoadBalancer | awk '{print $4}'``. 
If using Minikube, the IP may be Pending and require `minikube tunnel` - see [here](https://minikube.sigs.k8s.io/docs/handbook/accessing/#loadbalancer-access).
This IP can then be used to access the DB using psql or when running the component tests through the IDE. 

To run the component tests in a k8s pod:

```
make k8s-test KUBE_HOST=<KUBE_HOST>
```

To uninstall the chart, run

```
make k8s-uninstall-chart
```

## Deployments from CICD

There are 3 different environments which are defined through the standard pipeline templates. They need to be manually triggered in the Gitlab UI.

1. `dev` - a temporary (4 hours) deployment from a feature branch, using the artefacts built in the branch pipeline
2. `integration` - a permanent deployment from the main branch, using the latest version of the artefacts built in the main pipeline
3. `staging` - a permanent deployment of the latest published artefact from CAR

To find the URL for the environment, see the 'info' job of the CICD pipeline stage, which should output the URL alongside the status of the Kubernetes pods.
Generally the Swagger UI for the API should be available at  `https://k8s.stfc.skao.int/$KUBE_NAMESPACE/oda/api/v<MAJOR_VERSION>/ui/`

## Flask
To start the FastAPI server run the following command in a terminal bash prompt, in an env with the dependencies installed

```
fastapi run src/ska_db_oda/rest/app.py
```

Alternatively use the following command

```
make rest
```

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-db-oda/badge/?version=latest)](https://developer.skao.int/projects/ska-db-oda/en/latest/?badge=latest)

To rebuild the PlantUML and drawio diagrams after modification, from a
non-interactive session run

```
make diagrams
```

Further documentation, including a ``User Guide`` can be found in the 
``docs`` folder, or on [ReadTheDocs](https://developer.skao.int/projects/ska-db-oda/en/latest/index.html). 

To build the html version of the documentation, start 
from the ``ska-db-oda`` directory and first install the docs dependencies using 
``poetry install --only docs`` and then type ``make docs-build html``. Read the documentation by pointing your browser
at ``docs/build/html/index.html``.