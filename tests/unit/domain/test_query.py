import pytest
from dateutil import parser

from ska_db_oda.persistence.domain.errors import QueryParameterError
from ska_db_oda.persistence.domain.query import (
    DateQuery,
    MatchType,
    QueryParamsFactory,
    StatusQuery,
    UserQuery,
)


class TestQueryParamsFactory:
    @pytest.mark.parametrize(
        "qry_params,expected_query",
        [
            (
                {"created_after": "12.3.2017"},
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=parser.parse("12.3.2017"),
                ),
            ),
            (
                {"created_before": "20.3.2017"},
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    end=parser.parse("20.3.2017"),
                ),
            ),
            (
                {"created_after": "12.3.2017", "created_before": "20.3.2017"},
                DateQuery(
                    query_type=DateQuery.QueryType.CREATED_BETWEEN,
                    start=parser.parse("12.3.2017"),
                    end=parser.parse("20.3.2017"),
                ),
            ),
            (
                {"last_modified_after": "12.3.2017"},
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=parser.parse("12.3.2017"),
                ),
            ),
            (
                {"last_modified_before": "20.3.2017"},
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    end=parser.parse("20.3.2017"),
                ),
            ),
            (
                {
                    "last_modified_after": "12.3.2017",
                    "last_modified_before": "20.3.2017",
                },
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=parser.parse("12.3.2017"),
                    end=parser.parse("20.3.2017"),
                ),
            ),
            ({"user": "user123"}, UserQuery(user="user123")),
            ({"entity_id": "entity-1"}, StatusQuery(entity_id="entity-1")),
            (
                {"user": "user123", "match_type": "starts_with"},
                UserQuery(user="user123", match_type=MatchType.STARTS_WITH),
            ),
            (
                {"entity_id": "entity-1", "match_type": "starts_with"},
                StatusQuery(entity_id="entity-1", match_type=MatchType.STARTS_WITH),
            ),
            (
                {"entity_id": "entity-1", "match_type": "contains"},
                StatusQuery(entity_id="entity-1", match_type=MatchType.CONTAINS),
            ),
            (
                {"user": "user123", "match_type": "contains"},
                UserQuery(user="user123", match_type=MatchType.CONTAINS),
            ),
            (
                {
                    "user": None,
                    "created_after": None,
                    "created_before": None,
                    "last_modified_after": "12.3.2017",
                    "last_modified_before": "20.3.2017",
                },
                DateQuery(
                    query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
                    start=parser.parse("12.3.2017"),
                    end=parser.parse("20.3.2017"),
                ),
            ),
        ],
    )
    def test_get_query(self, qry_params, expected_query):
        query = QueryParamsFactory.from_dict(qry_params)
        assert query == expected_query

    def test_get_query_value_error(self):
        # with pytest.raises(ValueError):
        #     _ = QueryParamsFactory.from_dict({})

        with pytest.raises(QueryParameterError):
            _ = QueryParamsFactory.from_dict(
                {"user": "abc123", "created_before": "12.3.2019"}
            )

        with pytest.raises(QueryParameterError):
            _ = QueryParamsFactory.from_dict(
                {"last_modified_after": "14.4.2019", "created_before": "12.3.2019"}
            )
