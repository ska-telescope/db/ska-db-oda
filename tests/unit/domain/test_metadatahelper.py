"""
Tests for the common metadata functionality used by all implementations of the Repositories

The metadatamixin is mixed in to the AbstractRepository so all repositories share the same code. This means that
testing one Repository type is enough, but the tests are parameterised for the implementation of the SBRepository
for thoroughness.
"""
from datetime import timezone
from unittest import mock

import pytest

from ska_db_oda.persistence.domain.metadatamixin import datetime
from ska_db_oda.persistence.infrastructure.filesystem.repository import FilesystemBridge
from ska_db_oda.persistence.infrastructure.postgres.repository import PostgresBridge
from tests.conftest import TestDataFactory


@pytest.mark.parametrize(
    "repo_bridge_cls",
    [
        FilesystemBridge,
        PostgresBridge,
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
def test_update_metadata(mock_datetime, repo_bridge_cls):
    """
    Test the metadata is updated correctly when the validation passes
    """
    with mock.patch.object(repo_bridge_cls, "__init__", return_value=None):
        with mock.patch.object(
            repo_bridge_cls,
            "_get_latest_metadata",
            return_value=TestDataFactory.sbdefinition().metadata,
        ):
            now = datetime.now(tz=timezone.utc)
            mock_datetime.now.return_value = now

            repo_bridge = repo_bridge_cls()
            result = repo_bridge.update_metadata(
                entity=TestDataFactory.sbdefinition(), last_modified_by="NewUser"
            )

            assert result.metadata.version == 2
            assert result.metadata.last_modified_on == now
            assert result.metadata.last_modified_by == "NewUser"

            # Ensure object passed to method isn't mutated
            assert TestDataFactory.sbdefinition().metadata.version == 1
            assert (
                TestDataFactory.sbdefinition().metadata.last_modified_on
                == datetime.fromisoformat("2022-03-28T15:43:53.971548+00:00")
            )
            assert (
                TestDataFactory.sbdefinition().metadata.last_modified_by == "TestUser"
            )


@pytest.mark.parametrize(
    "repo_bridge_cls",
    [
        FilesystemBridge,
        PostgresBridge,
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
def test_new_metadata(mock_datetime, repo_bridge_cls):
    """
    Test the metadata is created correctly when the validation passes
    """
    with mock.patch.object(repo_bridge_cls, "__init__", return_value=None):
        with mock.patch.object(
            repo_bridge_cls, "_get_latest_metadata", return_value=None
        ):
            now = datetime.now(tz=timezone.utc)
            mock_datetime.now.return_value = now

            repo_bridge = repo_bridge_cls()
            result = repo_bridge.update_metadata(
                entity=TestDataFactory.sbdefinition(), last_modified_by="NewUser"
            )

            assert result.metadata.version == 1
            assert result.metadata.created_on == now
            assert result.metadata.created_by == "NewUser"
            assert result.metadata.last_modified_on == now
            assert result.metadata.last_modified_by == "NewUser"
