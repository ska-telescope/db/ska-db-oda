import json

from tests.conftest import DEFAULT_API_PATH
from tests.utils import assert_json_is_equal


def test_put_multiple(client, valid_multiple_payload):
    """
    Test successful PUT of an SBD
    """
    response = client.put(
        f"{DEFAULT_API_PATH}/",
        content=valid_multiple_payload,
    )

    assert response.status_code == 200
    assert_json_is_equal(valid_multiple_payload, json.dumps(response.json()))


def test_put_multiple_new_version(client, valid_multiple_payload):
    """
    Test successful PUT of updates to SBDefinitions
    """
    initial_response = client.put(
        f"{DEFAULT_API_PATH}/",
        content=valid_multiple_payload,
    )

    assert initial_response.status_code == 200
    assert_json_is_equal(valid_multiple_payload, json.dumps(initial_response.json()))

    # add update of same entities
    response = client.put(
        f"{DEFAULT_API_PATH}/",
        content=valid_multiple_payload,
    )

    assert response.status_code == 200
    assert_json_is_equal(valid_multiple_payload, json.dumps(response.json()))


def test_put_multiple_openapi_validation_error_400(client):
    """
    Verify that PUTting an invalid payload results in an OpenAPI validation response.
    """
    invalid_payload = {
        "ebs": {"should": "be an array"},
    }

    response = client.put(
        f"{DEFAULT_API_PATH}/",
        content=json.dumps(invalid_payload),
    )

    assert response.status_code == 422
    assert "Input should be a valid list" in response.json()["detail"][0]["msg"]


def test_put_sbds_duplicate_data_422(client, payload_with_duplicate_entities):
    """
    Test unsuccessful PUT of an SBD, caused by duplicate IDs
    """
    response = client.put(
        f"{DEFAULT_API_PATH}/", content=payload_with_duplicate_entities
    )

    assert response.status_code == 422
    assert "Unprocessable Entity, duplicate IDs in payload" in response.json()["detail"]
    assert "['eb-dup-123', 'eb-dup-123']" in response.json()["detail"]
