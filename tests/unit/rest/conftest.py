import os
from unittest import mock

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient

from ska_db_oda.rest.app import create_app
from ska_db_oda.rest.model import MultipleEntities
from tests.conftest import TestDataFactory


@pytest.fixture(name="test_app")
def test_app_fixture() -> FastAPI:
    """
    Fixture to configure a test app instance
    """
    return create_app(production=False)


@pytest.fixture()
def client(test_app: FastAPI, base_working_dir) -> TestClient:
    """
    Create a test client from the app instance, without running a live server
    """
    # patching ODA_DATA_DIR is required to get the default FilesystemRepository
    # constructor to point at a fixture-prepared path. Other repository
    # implementations will ignore the environment variable.
    with mock.patch.dict(os.environ, {"ODA_DATA_DIR": str(base_working_dir)}):
        yield TestClient(test_app, raise_server_exceptions=False)


@pytest.fixture
def valid_multiple_payload() -> str:
    """
    Returns a valid payload for the PUT multiple entities.
    """
    return MultipleEntities(
        sbds=[
            TestDataFactory.sbdefinition(sbd_id="sbd-123"),
            TestDataFactory.sbdefinition(sbd_id="sbd-456"),
        ],
        sbis=[
            TestDataFactory.sbinstance(sbi_id="sbi-123"),
            TestDataFactory.sbinstance(sbi_id="sbi-456"),
        ],
        ebs=[
            TestDataFactory.executionblock(eb_id="eb-123"),
            TestDataFactory.executionblock(eb_id="eb-456"),
        ],
        prjs=[
            TestDataFactory.project(prj_id="prj-123"),
            TestDataFactory.project(prj_id="prj-456"),
        ],
        prsls=[
            TestDataFactory.proposal(prsl_id="prsl-123"),
            TestDataFactory.proposal(prsl_id="prsl-456"),
        ],
    ).model_dump_json()


@pytest.fixture
def multiple_users_payload() -> str:
    """
    Returns a valid payload for the PUT multiple entities.
    """
    return MultipleEntities(
        sbds=[
            TestDataFactory.sbdefinition("123", last_modified_by="Hubble"),
            TestDataFactory.sbdefinition("456", last_modified_by="Bell Burnell"),
        ],
        sbis=[
            TestDataFactory.sbinstance(sbi_id="sbi-123"),
            TestDataFactory.sbinstance(sbi_id="sbi-456"),
        ],
        ebs=[
            TestDataFactory.executionblock(eb_id="eb-123"),
        ],
        prjs=[
            TestDataFactory.project(prj_id="prj-123"),
        ],
    ).model_dump_json()


@pytest.fixture
def payload_with_duplicate_entities() -> str:
    """
    Returns an invalid PUT payload containing entities with duplicate IDs.
    """
    return MultipleEntities(
        ebs=[
            TestDataFactory.executionblock("eb-dup-123"),
            TestDataFactory.executionblock("eb-dup-123"),
        ]
    ).model_dump_json()
