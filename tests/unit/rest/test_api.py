import json
from datetime import datetime
from time import time_ns
from unittest import TestCase, mock

import pytest
from dateutil.tz import tzlocal
from ska_oso_pdm._shared import Metadata, PythonArguments
from ska_oso_pdm.entity_status_history import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)
from ska_oso_pdm.execution_block import RequestResponse, ResponseWrapper

from ska_db_oda.constant import NOT_FOUND_MESSAGE, SBIS
from ska_db_oda.persistence.domain import CODEC, get_identifier
from ska_db_oda.persistence.domain.errors import ODANotFound
from tests.conftest import DEFAULT_API_PATH, TestDataFactory
from tests.utils import assert_json_is_equal

test_case = TestCase()


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition(sbd_id="test-id-123")),
        ("sbis", TestDataFactory.sbinstance(sbi_id="test-id-123")),
        ("ebs", TestDataFactory.executionblock(eb_id="test-id-123")),
        ("prjs", TestDataFactory.project(prj_id="test-id-123")),
        ("prsls", TestDataFactory.proposal(prsl_id="test-id-123")),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_get(mock_skuid_fetch, mock_datetime, client, resource, entity):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    mock_skuid_fetch.return_value = test_id

    # the server needs an entity before it can be retrieved
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert post_response.status_code == 200

    entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )
    response = client.get(f"{DEFAULT_API_PATH}/{resource}/{test_id}")
    assert response.status_code == 200
    assert_json_is_equal(response.text, CODEC.dumps(entity))


@pytest.mark.parametrize(
    "parent_resource,resource, parent_entity,entity",
    [
        (
            "ebs",
            "status/history/ebs",
            TestDataFactory.executionblock(eb_id="test-id-123"),
            TestDataFactory.eb_status(eb_ref="test-id-123"),
        ),
        (
            "sbds",
            "status/history/sbds",
            TestDataFactory.sbdefinition(sbd_id="test-id-123"),
            TestDataFactory.sbd_status(sbd_ref="test-id-123"),
        ),
        (
            "sbis",
            "status/history/sbis",
            TestDataFactory.sbinstance(sbi_id="test-id-123"),
            TestDataFactory.sbi_status(sbi_ref="test-id-123"),
        ),
        (
            "prjs",
            "status/history/prjs",
            TestDataFactory.project(prj_id="test-id-123"),
            TestDataFactory.prj_status(prj_ref="test-id-123"),
        ),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_get_status_history(
    mock_skuid_fetch,
    mock_datetime,
    client,
    parent_resource,
    resource,
    parent_entity,
    entity,
):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    mock_skuid_fetch.return_value = test_id
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{parent_resource}", content=CODEC.dumps(parent_entity)
    )
    assert post_response.status_code == 200

    entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )

    response = client.get(f"{DEFAULT_API_PATH}/{resource}?entity_id={test_id}")

    assert response.status_code == 200
    assert response.json()[0]["current_status"] == entity.current_status


@pytest.mark.parametrize(
    "parent_resource,resource, parent_entity,entity",
    [
        (
            "ebs",
            "status/history/ebs",
            TestDataFactory.executionblock(eb_id="test-id-123"),
            TestDataFactory.eb_status(eb_ref="test-id-123"),
        ),
        (
            "sbds",
            "status/history/sbds",
            TestDataFactory.sbdefinition(sbd_id="test-id-123"),
            TestDataFactory.sbd_status(sbd_ref="test-id-123"),
        ),
        (
            "sbis",
            "status/history/sbis",
            TestDataFactory.sbinstance(sbi_id="test-id-123"),
            TestDataFactory.sbi_status(sbi_ref="test-id-123"),
        ),
        (
            "prjs",
            "status/history/prjs",
            TestDataFactory.project(prj_id="test-id-123"),
            TestDataFactory.prj_status(prj_ref="test-id-123"),
        ),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_get_status_history_with_version(
    mock_skuid_fetch,
    mock_datetime,
    client,
    parent_resource,
    resource,
    parent_entity,
    entity,
):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    mock_skuid_fetch.return_value = test_id
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{parent_resource}", content=CODEC.dumps(parent_entity)
    )
    assert post_response.status_code == 200

    entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )

    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?entity_id={test_id}&version=1"
    )

    assert response.status_code == 200
    assert response.json()[0]["current_status"] == entity.current_status


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
        ("status/history/ebs", TestDataFactory.eb_status()),
        ("status/history/sbds", TestDataFactory.sbd_status()),
        ("status/history/sbis", TestDataFactory.sbi_status()),
        ("status/history/prjs", TestDataFactory.prj_status()),
    ],
)
def test_get_not_found_404(client, resource, entity):
    """
    Tests if a 404 is returned on attempted retrieval of a non-existent entity via the
    GET endpoint
    """
    url = f"{DEFAULT_API_PATH}/{resource}/entity not found"
    if isinstance(
        entity,
        (SBDStatusHistory, SBIStatusHistory, OSOEBStatusHistory, ProjectStatusHistory),
    ):
        url = f"{DEFAULT_API_PATH}/{resource}?entity_id=entity not found"
    response = client.get(url)

    assert response.status_code == 404
    assert (
        response.json()["detail"]
        == "The requested identifier entity not found could not be found."
    )


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
        ("ebs", TestDataFactory.eb_status()),
        ("sbds", TestDataFactory.sbd_status()),
        ("sbis", TestDataFactory.sbi_status()),
        ("prjs", TestDataFactory.prj_status()),
    ],
)
def test_get_internal_server_error_500(client, resource, entity):
    """
    Tests if an Internal Server Error is returned for an unexpected error.
    """
    url = f"{DEFAULT_API_PATH}/{resource}/entity-does-not-exist"
    if isinstance(
        entity,
        (SBDStatusHistory, SBIStatusHistory, OSOEBStatusHistory, ProjectStatusHistory),
    ):
        url = f"{DEFAULT_API_PATH}/status/history/{resource}?entity_id=entity-does-not-exist"

    with mock.patch(
        f"ska_db_oda.rest.api.{resource}.oda.uow", return_value="Not a UoW"
    ):
        response = client.get(url)

    assert response.status_code == 500
    assert "AttributeError" in response.json()["detail"]["description"]
    assert response.json()["detail"]["traceback"] is not None


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_post(mock_skuid_fetch, client, resource, entity):
    """
    Test successful POST of an entity to the given resource URI.
    It should call SKUID for the new ID and persist the entity using the UoW
    """
    test_entity_id = "test-id-123"
    mock_skuid_fetch.return_value = test_entity_id
    response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert response.status_code == 200
    assert response.json()[f"{resource[:-1]}_id"] == test_entity_id

    # Check ID was fetched from SKUID
    mock_skuid_fetch.assert_called_once_with(resource[:-1])

    # Check the entity was saved to the ODA
    result = client.get(f"{DEFAULT_API_PATH}/{resource}/{test_entity_id}")

    assert result.status_code == 200


@pytest.mark.parametrize("resource", ["sbds", "sbis", "ebs", "prjs"])
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_post_validation_error(mock_skuid_fetch, client, resource):
    """
    Test a request to POST an entity handles an exception thrown from the deserialization and validation of the entity, and wraps it in an appropriate response
    """
    test_entity_id = "test-id-123"
    mock_skuid_fetch.return_value = test_entity_id
    entity_json = """{"metadata":"not_metadata"}"""
    response = client.post(f"{DEFAULT_API_PATH}/{resource}", content=entity_json)

    assert response.status_code == 422
    assert (
        "Input should be a valid dictionary or object to extract fields from"
        in response.json()["detail"][0]["msg"]
    )


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_post_internal_server_error(mock_skuid_fetch, client, resource, entity):
    """
    Test a request to POST an entity handles a general exception and returns a formatted response
    """
    mock_skuid_fetch.side_effect = RuntimeError("no skuid available")

    response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert response.status_code == 500
    assert "no skuid available" in response.json()["detail"]["description"]


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition(sbd_id="test-id-123")),
        ("sbis", TestDataFactory.sbinstance(sbi_id="test-id-123")),
        ("ebs", TestDataFactory.executionblock(eb_id="test-id-123")),
        ("prjs", TestDataFactory.project(prj_id="test-id-123")),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
def test_put(mock_datetime, mock_skuid_fetch, client, resource, entity):
    """
    Test successful PUT of an entity to the given resource URI
    """
    mock_datetime.now.return_value = datetime.fromisoformat(
        "2022-03-28T15:43:53.971548+00:00"
    )
    test_entity_id = "test-id-123"
    mock_skuid_fetch.return_value = test_entity_id
    response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert response.status_code == 200

    response = client.put(
        f"{DEFAULT_API_PATH}/{resource}/{test_entity_id}", content=CODEC.dumps(entity)
    )

    assert response.status_code == 200

    assert_json_is_equal(
        response.text,
        CODEC.dumps(entity),
        exclude_paths=["root['metadata']"],
    )


@pytest.mark.parametrize(
    "entity_id, resource, parent_entity, entity",
    [
        (
            "ebs",
            "status/ebs",
            TestDataFactory.executionblock(eb_id="test-id-123"),
            TestDataFactory.eb_status(eb_ref="test-id-123"),
        ),
        (
            "sbds",
            "status/sbds",
            TestDataFactory.sbdefinition(sbd_id="test-id-123"),
            TestDataFactory.sbd_status(sbd_ref="test-id-123"),
        ),
        (
            "sbis",
            "status/sbis",
            TestDataFactory.sbinstance(sbi_id="test-id-123"),
            TestDataFactory.sbi_status(sbi_ref="test-id-123"),
        ),
        (
            "prjs",
            "status/prjs",
            TestDataFactory.project(prj_id="test-id-123"),
            TestDataFactory.prj_status(prj_ref="test-id-123"),
        ),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_put_status_history(
    mock_skuid_fetch, mock_datetime, client, entity_id, resource, parent_entity, entity
):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    mock_skuid_fetch.return_value = test_id

    post_response = client.post(
        f"{DEFAULT_API_PATH}/{entity_id}", content=CODEC.dumps(parent_entity)
    )

    assert post_response.status_code == 200
    status_entity = json.loads(CODEC.dumps(entity))
    status_entity["version"] = entity.metadata.version

    response = client.put(
        f"{DEFAULT_API_PATH}/{resource}/{test_id}", content=json.dumps(status_entity)
    )

    assert response.status_code == 200


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("prjs", TestDataFactory.project()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("status/sbds", TestDataFactory.sbd_status()),
        ("status/sbis", TestDataFactory.sbi_status()),
        ("status/ebs", TestDataFactory.eb_status()),
        ("status/prjs", TestDataFactory.prj_status()),
    ],
)
def test_put_not_found_404(client, resource, entity):
    """
    Test a request to PUT an entity where the identifier is not already present in the ODA returns a NOT FOUND.
    """

    if "status" in resource:
        status_entity = json.loads(CODEC.dumps(entity))
        status_entity["version"] = entity.metadata.version
        response = client.put(
            f"{DEFAULT_API_PATH}/{resource}/{get_identifier(entity)}",
            content=json.dumps(status_entity),
        )
    else:
        response = client.put(
            f"{DEFAULT_API_PATH}/{resource}/{get_identifier(entity)}",
            content=CODEC.dumps(entity),
        )

    assert response.status_code == 404

    assert (
        response.json()["detail"]
        == f"The requested identifier {get_identifier(entity)} could not be found."
    )


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
    ],
)
def test_put_openapi_validation_error_422(client, resource, entity):
    """
    Test unsuccessful PUT of an entity which does not conform to the schema
    """

    invalid_request_body = {
        "interface": "some interface",
        "metadata": "not metadata",
    }

    response = client.put(
        f"{DEFAULT_API_PATH}/{resource}/{get_identifier(entity)}",
        content=json.dumps(invalid_request_body),
    )

    assert response.status_code == 422
    assert (
        "Input should be a valid dictionary or object to extract fields from"
        == response.json()["detail"][0]["msg"]
    )


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
    ],
)
def test_put_mismatched_ids_422(client, resource, entity):
    """
    Tests if a mismatch between endpoint and entity ID
    provides an UNPROCESSABLE_ENTITY response
    """

    incorrect_id = "entity-mvp01-20200325-mismatch"

    response = client.put(
        f"{DEFAULT_API_PATH}/{resource}/{incorrect_id}", content=CODEC.dumps(entity)
    )

    assert response.status_code == 422
    assert (
        response.json()["detail"]
        == "There is a mismatch between the identifier for the endpoint"
        " entity-mvp01-20200325-mismatch and the JSON payload"
        f" {get_identifier(entity)}"
    )


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("status/sbds", TestDataFactory.sbd_status()),
        ("status/sbis", TestDataFactory.sbi_status()),
        ("status/ebs", TestDataFactory.eb_status()),
        ("status/prjs", TestDataFactory.prj_status()),
    ],
)
def test_put_status_history_mismatched_ids_422(client, resource, entity):
    """
    Tests if a mismatch between endpoint and entity ID
    provides an UNPROCESSABLE_ENTITY response
    """

    incorrect_id = "entity-mvp01-20200325-mismatch"

    response = client.put(
        f"{DEFAULT_API_PATH}/{resource}/{incorrect_id}", content=CODEC.dumps(entity)
    )

    assert response.status_code == 422
    assert (
        response.json()["detail"]
        == "There is a mismatch between the identifier for the endpoint"
        f" {incorrect_id} and the JSON payload {get_identifier(entity)}"
    )


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
    ],
)
def test_put_internal_server_error_500(client, resource, entity):
    """
    Test unsuccessful PUT of an entity which contains invalid metadata
    """
    entity.metadata.version = 10

    with mock.patch(
        f"ska_db_oda.rest.api.{resource}.oda.uow", return_value="Not an ODA object"
    ):
        response = client.put(
            f"{DEFAULT_API_PATH}/{resource}/{get_identifier(entity)}",
            content=CODEC.dumps(entity),
        )

    assert response.status_code == 500
    assert "AttributeError" in response.json()["detail"]["description"]


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("status/sbds", TestDataFactory.sbd_status()),
        ("status/sbis", TestDataFactory.sbi_status()),
        ("status/ebs", TestDataFactory.eb_status()),
        ("status/prjs", TestDataFactory.prj_status()),
    ],
)
def test_put_status_history_internal_server_error_500(client, resource, entity):
    """
    Test unsuccessful PUT of an entity which raises an error
    """
    entity.metadata.version = 10
    with mock.patch(
        f"ska_db_oda.rest.api.{resource.split('/')[-1]}.oda.uow",
        return_value="Not an ODA object",
    ):
        response = client.put(
            f"{DEFAULT_API_PATH}/{resource}/{get_identifier(entity)}",
            content=CODEC.dumps(entity),
        )

    assert response.status_code == 500
    assert "AttributeError('__enter__')" == response.json()["detail"]["description"]


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition(sbd_id=None)),
        ("sbis", TestDataFactory.sbinstance(sbi_id=None)),
        ("ebs", TestDataFactory.executionblock(eb_id=None)),
        ("prjs", TestDataFactory.project(prj_id=None)),
        ("prsls", TestDataFactory.proposal(prsl_id=None)),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_query_by_user_returns_ids_200(mock_skuid_fetch, client, resource, entity):
    """
    Test a query with 2 results returns both IDs
    """
    test_id = f"test-id-{time_ns()}"
    mock_skuid_fetch.return_value = test_id
    # the server needs an entity before it can be retrieved
    setup_response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert setup_response.status_code == 200

    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?user=Default&match_type=starts_with"
    )

    assert response.status_code == 200

    assert [test_id] == [res[f"{resource[:-1]}_id"] for res in response.json()]


@pytest.mark.parametrize(
    "resource",
    ["sbds", "sbis", "ebs", "prjs", "prsls"],
)
def test_query_by_created_on_returns_ids_200(client, resource, valid_multiple_payload):
    """
    Test a query with 2 results returns both IDs
    """
    setup_response = client.put(f"{DEFAULT_API_PATH}/", content=valid_multiple_payload)

    assert setup_response.status_code == 200

    response = client.get(f"{DEFAULT_API_PATH}/{resource}?created_after=1900-01-01")

    assert response.status_code == 200
    test_case.assertCountEqual(
        [res[f"{resource[:-1]}_id"] for res in response.json()],
        [f"{resource[:-1]}-123", f"{resource[:-1]}-456"],
    )

    response = client.get(f"{DEFAULT_API_PATH}/{resource}?created_before=1900-01-01")

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.parametrize(
    "resource, entity",
    [
        ("sbds", TestDataFactory.sbdefinition(sbd_id=None)),
        ("sbis", TestDataFactory.sbinstance(sbi_id=None)),
        ("ebs", TestDataFactory.executionblock(eb_id=None)),
        ("prjs", TestDataFactory.project(prj_id=None)),
        ("prsls", TestDataFactory.proposal(prsl_id=None)),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_query_by_modified_on_returns_ids_200(
    mock_skuid_fetch, client, resource, entity
):
    """
    Test a query with 2 results returns both IDs
    """
    test_id = f"test-id-{time_ns()}"
    mock_skuid_fetch.return_value = test_id
    # the server needs an entity before it can be retrieved
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert post_response.status_code == 200

    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?last_modified_after=1900-01-01"
    )
    assert response.status_code == 200
    assert test_id in [res[f"{resource[:-1]}_id"] for res in response.json()]

    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?last_modified_before=1900-01-01"
    )

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.parametrize(
    "resource",
    ["sbds", "sbis", "ebs", "prjs", "prsls"],
)
def test_query_by_user_no_results_200(client, resource):
    """
    Test a query with no results returns an empty list
    """
    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?user=Tom&match_type=starts_with"
    )

    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.parametrize(
    "resource",
    ["sbds", "sbis", "ebs", "prjs", "prsls"],
)
def test_query_no_params_422(client, resource):
    """
    Test a query with no params returns an error response.

    Eventually the implementation should return all entites, with paging.
    """
    response = client.get(f"{DEFAULT_API_PATH}/{resource}")

    assert response.status_code == 422
    assert (
        "Parameters are missing or not currently supported for querying."
        == response.json()["detail"]
    )


@pytest.mark.parametrize(
    "resource",
    ["sbds", "sbis", "ebs", "prjs", "prsls"],
)
def test_query_with_bad_datetime_422(client, resource):
    """
    Test a query with a unparsable datetime returns an error response
    """
    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?last_modified_after=not_a_date"
    )

    assert response.status_code == 422
    assert (
        "Input should be a valid datetime or date"
        in response.json()["detail"][0]["msg"]
    )


class TestExecutionBlockResources:
    @mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
    def test_create_eb_returns_id_on_success(self, mock_skuid_fetch, client):
        test_eb_id = "test-id-123"
        mock_skuid_fetch.return_value = test_eb_id
        response = client.post(
            f"{DEFAULT_API_PATH}/ebs", content=json.dumps({"telescope": "ska_mid"})
        )

        assert response.status_code == 200
        assert response.json()["eb_id"] == test_eb_id

        # Check ID was fetched from SKUID
        mock_skuid_fetch.assert_called_once_with("eb")

        # Check the empty EB was saved to the ODA
        result = client.get(f"{DEFAULT_API_PATH}/ebs/{test_eb_id}")

        assert result.status_code == 200

    @mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
    def test_create_eb_returns_error_on_skuid_error(self, mock_skuid_fetch, client):
        mock_skuid_fetch.side_effect = RuntimeError(ValueError("test error"))
        response = client.post(
            f"{DEFAULT_API_PATH}/ebs", content=json.dumps({"telescope": "ska_mid"})
        )

        assert response.status_code == 500
        assert (
            "RuntimeError(ValueError('test error'))"
            == response.json()["detail"]["description"]
        )

    @mock.patch("ska_db_oda.rest.api.ebs.oda.uow")
    def test_add_request_response_success(self, mock_oda, client):
        test_eb_id = "test-id-123"
        test_eb = TestDataFactory.executionblock(eb_id=test_eb_id)
        test_eb.request_responses = []
        uow_mock = mock.MagicMock()
        uow_mock.ebs.get.return_value = test_eb
        uow_mock.ebs.add.return_value = test_eb
        mock_oda().__enter__.return_value = uow_mock

        SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")

        request_response = RequestResponse(
            request="ska_oso_scripting.functions.devicecontrol.release_all_resources",
            request_args=PythonArguments(kwargs={"subarray_id": "1"}),
            status="OK",
            response=ResponseWrapper(result="this is a result"),
            request_sent_at=SAMPLE_DATETIME,
            response_received_at=SAMPLE_DATETIME,
        )

        response = client.patch(
            f"{DEFAULT_API_PATH}/ebs/{test_eb_id}/request_response",
            content=CODEC.dumps(request_response),
            headers={"Content-type": "application/json"},
        )

        assert response.status_code == 200
        assert_json_is_equal(response.text, CODEC.dumps(test_eb))
        assert test_eb.request_responses == [request_response]

        assert 1 == uow_mock.ebs.get.call_count
        assert 1 == uow_mock.ebs.add.call_count

    @mock.patch("ska_db_oda.rest.api.ebs.oda.uow")
    def test_add_request_response_not_found(self, mock_oda, client):
        uow_mock = mock.MagicMock()
        uow_mock.ebs.get.side_effect = ODANotFound(
            message="this is a not found message"
        )
        mock_oda().__enter__.return_value = uow_mock

        response = client.patch(
            f"{DEFAULT_API_PATH}/ebs/entity-does-not-exist/request_response",
            content=CODEC.dumps(RequestResponse()),
            headers={"Content-type": "application/json"},
        )

        assert response.status_code == 404
        assert response.json()["detail"] == "this is a not found message"

        uow_mock.ebs.get.assert_called_once()


@pytest.mark.parametrize(
    "resource, entity, entity_id, entity_field",
    [
        (
            "sbds",
            TestDataFactory.sbdefinition(sbd_id="test-id-123"),
            "test-id-123",
            "sbd_id",
        ),
        (
            "sbis",
            TestDataFactory.sbinstance(sbi_id="test-id-123"),
            "test-id-123",
            "sbi_id",
        ),
        (
            "ebs",
            TestDataFactory.executionblock(eb_id="test-id-123"),
            "test-id-123",
            "eb_id",
        ),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_entity_id_search(
    mock_skuid_fetch, mock_datetime, client, resource, entity, entity_id, entity_field
):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    mock_skuid_fetch.return_value = test_id

    setattr(entity, entity_field, "test-id-123")
    # the server needs an entity before it can be retrieved
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{resource}", content=CODEC.dumps(entity)
    )

    assert post_response.status_code == 200

    entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )
    response = client.get(f"{DEFAULT_API_PATH}/{resource}/{entity_id}")

    assert response.status_code == 200
    assert_json_is_equal(json.dumps(response.json()), CODEC.dumps(entity))


@pytest.mark.parametrize(
    "parent_resource,resource, parent_entity,entity,entity_id,entity_field",
    [
        (
            "ebs",
            "status/history/ebs",
            TestDataFactory.executionblock(eb_id="test-id-123"),
            TestDataFactory.eb_status(eb_ref="test-id-123"),
            "test-id-123",
            "eb_ref",
        ),
        (
            "sbds",
            "status/history/sbds",
            TestDataFactory.sbdefinition(sbd_id="test-id-123"),
            TestDataFactory.sbd_status(sbd_ref="test-id-123"),
            "test-id-123",
            "sbd_ref",
        ),
        (
            "sbis",
            "status/history/sbis",
            TestDataFactory.sbinstance(sbi_id="test-id-123"),
            TestDataFactory.sbi_status(sbi_ref="test-id-123"),
            "test-id-123",
            "sbi_ref",
        ),
        (
            "prjs",
            "status/history/prjs",
            TestDataFactory.project(prj_id="test-id-123"),
            TestDataFactory.prj_status(prj_ref="test-id-123"),
            "test-id-123",
            "prj_ref",
        ),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_entity_id_search_with_status(
    mock_skuid_fetch,
    mock_datetime,
    client,
    parent_resource,
    resource,
    parent_entity,
    entity,
    entity_id,
    entity_field,
):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    mock_skuid_fetch.return_value = test_id

    setattr(entity, entity_field, "test-id-123")
    # the server needs an entity before it can be retrieved
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{parent_resource}", content=CODEC.dumps(parent_entity)
    )
    assert post_response.status_code == 200
    entity.metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )
    response = client.get(f"{DEFAULT_API_PATH}/{resource}?entity_id={entity_id}")
    assert response.status_code == 200

    json_response1 = json.loads(response.text)
    entity_dict = json.loads(CODEC.dumps(entity))

    assert json_response1[0]["current_status"] == entity_dict["current_status"]
    assert json_response1[0]["previous_status"] == entity_dict["previous_status"]

    response = client.get(
        f"{DEFAULT_API_PATH}/{resource}?entity_id={entity_id}&starts_with='test'"
    )
    assert response.status_code == 200
    json_response2 = json.loads(response.text)
    entity_dict = json.loads(CODEC.dumps(entity))
    assert json_response2[0]["current_status"] == entity_dict["current_status"]
    assert json_response2[0]["previous_status"] == entity_dict["previous_status"]


@pytest.mark.parametrize(
    "resources, entities",
    [
        (
            "sbis,ebs",
            (
                TestDataFactory.sbinstance(sbi_id="test-id-123"),
                TestDataFactory.executionblock(eb_id="test-id-123"),
            ),
        ),
    ],
)
@mock.patch("ska_db_oda.persistence.domain.metadatamixin.datetime")
@mock.patch("ska_db_oda.persistence.domain.skuid.fetch_skuid")
def test_get_with_associated_entity_relationship(
    mock_skuid_fetch, mock_datetime, client, resources, entities
):
    """
    Test successful retrieval of an entity via the GET endpoint
    """
    now = datetime.now(tz=tzlocal())
    mock_datetime.now.return_value = now
    test_id = "test-id-123"
    entities[1].sbi_ref = "test-id-123"
    mock_skuid_fetch.return_value = test_id
    resources = resources.split(",")
    # the server needs an entity before it can be retrieved
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{resources[0]}", content=CODEC.dumps(entities[0])
    )

    assert post_response.status_code == 200
    post_response = client.post(
        f"{DEFAULT_API_PATH}/{resources[1]}", content=CODEC.dumps(entities[1])
    )

    assert post_response.status_code == 200
    resources_sbi = SBIS
    response = client.get(
        f"{DEFAULT_API_PATH}/{resources[1]}/{test_id}/{resources_sbi}/"
    )
    entities[0].metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )
    entities[1].metadata = Metadata(
        version=1,
        created_on=now,
        created_by="DefaultUser",
        last_modified_on=now,
        last_modified_by="DefaultUser",
    )
    assert response.status_code == 200
    assert json.loads(response.text), json.loads(CODEC.dumps(entities[0]))
