from os import getenv

# The ODA interfaces (and therefore API service) can be configured to store data either in postgres instance
# or on the filesystem. Similarly, the CLI can be configured to access an ODA API or the filesystem of the process it is running on.
# If TEST_FILESYSTEM_IMPL is true then the component tests will run against these filesystem implementations
TEST_FILESYSTEM_IMPL = getenv("TEST_FILESYSTEM_IMPL", "false") == "true"
