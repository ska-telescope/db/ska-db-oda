"""
Component level tests for ska-db-oda

These will run from a test pod inside a kubernetes cluster, making requests
to a deployment of ska-db-oda in the same cluster.
They should be agnostic about the backend type of the deployed ska-db-oda.
"""
import json
import time
from datetime import datetime, timezone

import pytest
import requests
from ska_oso_pdm import OSOExecutionBlock
from ska_oso_pdm.execution_block import (
    PythonArguments,
    RequestResponse,
    ResponseWrapper,
)

from ska_db_oda.persistence.domain import CODEC, get_identifier
from tests.conftest import ODA_URL, TestDataFactory
from tests.utils import assert_json_is_equal


@pytest.mark.post_deployment
@pytest.mark.parametrize(
    "resource",
    ["sbds", "sbis", "ebs", "prjs", "prsls"],
)
@pytest.mark.xray("XTP-73525")
def test_entity_get_not_found(resource):
    """
    Test that the GET /sbds/{identifier} path returns
    404 when the SBD is not found in the ODA
    """
    response = requests.get(f"{ODA_URL}/{resource}/123")
    assert (
        response.json()["detail"] == "The requested identifier 123 could not be found."
    )
    assert response.status_code == 404


@pytest.mark.post_deployment
@pytest.mark.parametrize(
    "resource,test_entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("prjs", TestDataFactory.project()),
        ("prsls", TestDataFactory.proposal()),
    ],
)
@pytest.mark.xray("XTP-73526")
def test_entity_add_versioned_entity_then_get(resource, test_entity):
    """
    Test that
     1. POST /{resource} handles the request and returns the correct response with a new identifier
     2. Subsequent requests to PUT /{resource}/{identifier} will create a new version
     3. GET /{resource}/{identifier} handles the request and returns the latest version
    """
    entity_json = CODEC.dumps(test_entity)

    response = requests.post(
        f"{ODA_URL}/{resource}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    assert response.status_code == 200

    # Get the id that has been fetched from skuid and stored as part of the entity
    created_entity = CODEC.loads(test_entity.__class__, response.content)
    entity_id = get_identifier(created_entity)
    assert entity_id

    entity_json = CODEC.dumps(created_entity)

    response = requests.put(
        f"{ODA_URL}/{resource}/{entity_id}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    assert response.status_code == 200

    response = requests.get(f"{ODA_URL}/{resource}/{entity_id}")

    assert response.status_code == 200
    assert_json_is_equal(
        response.content, entity_json, exclude_paths="root['metadata']"
    )
    assert json.loads(response.content)["metadata"]["version"] == 2


@pytest.mark.post_deployment
@pytest.mark.parametrize(
    "resource,test_entity",
    [
        ("sbis", TestDataFactory.sbinstance(sbi_id=f"sbi-{time.time_ns()}")),
        ("ebs", TestDataFactory.executionblock(eb_id=f"eb-{time.time_ns()}")),
    ],
)
@pytest.mark.xray("XTP-73527")
def test_entity_add_none_versioned_entity_then_get(resource, test_entity):
    """
    Test that
     1. POST /{resource} handles the request and returns the correct response with a new identifier
     2. Subsequent requests to PUT /{resource}/{identifier} will update version 1
     3. GET /{resource}/{identifier} handles the request and returns version 1
    """
    # make SBD ID unique so that test is not dependent on ODA state
    entity_json = CODEC.dumps(test_entity)

    response = requests.post(
        f"{ODA_URL}/{resource}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    assert response.status_code == 200
    # Get the id that has been fetched from skuid and stored as part of the entity
    created_entity = CODEC.loads(test_entity.__class__, response.content)
    entity_id = get_identifier(created_entity)
    assert entity_id

    entity_json = CODEC.dumps(created_entity)

    response = requests.put(
        f"{ODA_URL}/{resource}/{entity_id}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    assert response.status_code == 200

    response = requests.get(f"{ODA_URL}/{resource}/{entity_id}")

    assert response.status_code == 200
    assert_json_is_equal(
        response.content, entity_json, exclude_paths="root['metadata']"
    )
    assert json.loads(response.content)["metadata"]["version"] == 1


@pytest.mark.post_deployment
@pytest.mark.parametrize(
    "resource,test_entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("prjs", TestDataFactory.project()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prsls", TestDataFactory.proposal()),
    ],
)
@pytest.mark.xray("XTP-73529")
def test_entity_add_new_entity_then_get(resource, test_entity):
    """
    Test that
     1. POST /{resource} creates a new entity with an ID fetched from SKUID
     3. GET /{resource}/{identifier} handles the request and returns this new version 1
    """
    entity_json = CODEC.dumps(test_entity)

    response = requests.post(
        f"{ODA_URL}/{resource}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    assert response.status_code == 200

    # Get the id that has been fetched from skuid and stored as part of the entity
    created_entity = CODEC.loads(test_entity.__class__, response.content)
    entity_id = get_identifier(created_entity)
    assert entity_id

    entity_json = CODEC.dumps(created_entity)

    response = requests.get(f"{ODA_URL}/{resource}/{entity_id}")

    assert response.status_code == 200
    assert_json_is_equal(
        response.content,
        entity_json,
        exclude_paths=["root['metadata']"],
    )
    assert json.loads(response.content)["metadata"]["version"] == 1


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73530")
def test_add_multiple_then_get():
    """
    Test that the POST / to add multiple entities receives the request
    and returns the correct response
    """
    num_sbds = 10
    epoch = time.time_ns()

    sbd_id_to_json = {}
    for i in range(num_sbds):
        sbd_id = f"sbd-{epoch}-{i}"
        sbd = TestDataFactory.sbdefinition(sbd_id=sbd_id)
        sbd_json = CODEC.dumps(sbd)
        sbd_id_to_json[sbd_id] = sbd_json

    payload = dict(sbds=[json.loads(sbd_json) for sbd_json in sbd_id_to_json.values()])

    put_response = requests.put(
        ODA_URL,
        data=json.dumps(payload),
        headers={"Content-type": "application/json"},
    )

    assert put_response.status_code == 200

    for sbd_id, sbd_json in sbd_id_to_json.items():
        response = requests.get(f"{ODA_URL}/sbds/{sbd_id}")
        assert response.status_code == 200

        assert_json_is_equal(
            response.content,
            sbd_json,
            exclude_paths="root['metadata']",
        )


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73531")
def test_add_multiple_without_id():
    """
    Test that the PUT / to add multiple entities will fetch identifiers
    from skuid if they are not given in the object
    """
    payload = dict(
        sbds=[json.loads(CODEC.dumps(TestDataFactory.sbdefinition(sbd_id=None)))]
    )

    put_response = requests.put(
        ODA_URL,
        data=json.dumps(payload),
        headers={"Content-type": "application/json"},
    )

    assert put_response.status_code == 200
    fetched_id = json.loads(put_response.content)["sbds"][0]["sbd_id"]

    response = requests.get(f"{ODA_URL}/sbds/{fetched_id}")
    assert response.status_code == 200


@pytest.mark.post_deployment
@pytest.mark.parametrize(
    "resource,test_entity",
    [
        ("sbds", TestDataFactory.sbdefinition()),
        ("sbis", TestDataFactory.sbinstance()),
        ("ebs", TestDataFactory.executionblock()),
        ("prjs", TestDataFactory.project()),
        ("prsls", TestDataFactory.proposal()),
    ],
)
@pytest.mark.xray("XTP-73532")
def test_query(resource, test_entity):
    """
    Test that the GET /resource path with parameters returns the correct response
    for the query
    """

    now = datetime.now(tz=timezone.utc)
    entity_json = CODEC.dumps(test_entity)

    # Arrange the test data to query
    response = requests.post(
        f"{ODA_URL}/{resource}",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )
    assert response.status_code == 200

    entity_id = get_identifier(CODEC.loads(test_entity.__class__, response.content))
    id_field = f"{resource[:-1]}_id"

    # Act
    response = requests.get(f"{ODA_URL}/{resource}?last_modified_after={now.date()}")

    # Assert
    assert response.status_code == 200
    fetched_ids = [entity[id_field] for entity in response.json()]
    assert entity_id in fetched_ids


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73533")
def test_add_request_response_to_eb():
    """
    Test that a POST /ebs/<eb_id>/request_responses request persists the request_response in the EB.
    """
    create_response = requests.post(
        f"{ODA_URL}/ebs",
        data=json.dumps({"telescope": "ska_mid"}),
        headers={"Content-type": "application/json"},
    )
    assert "eb_id" in create_response.json()
    eb_id = create_response.json()["eb_id"]

    SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")
    request_response = RequestResponse(
        request="ska_oso_scripting.functions.devicecontrol.release_all_resources",
        request_args=PythonArguments(kwargs={"subarray_id": "1"}),
        status="OK",
        response=ResponseWrapper(result="this is a result"),
        request_sent_at=SAMPLE_DATETIME,
        response_received_at=SAMPLE_DATETIME,
    )

    entity_json = CODEC.dumps(request_response)
    response = requests.patch(
        f"{ODA_URL}/ebs/{eb_id}/request_response",
        data=entity_json,
        headers={"Content-type": "application/json"},
    )

    expected_response = CODEC.dumps(
        OSOExecutionBlock(
            eb_id=eb_id, telescope="ska_mid", request_responses=[request_response]
        )
    )
    assert_json_is_equal(
        response.content, expected_response, exclude_paths="root['metadata']"
    )
    assert json.loads(response.content)["metadata"]["version"] == 1
