# pylint: disable=redefined-outer-name,protected-access
from pathlib import Path
from typing import List

import pytest
from psycopg import sql
from psycopg.errors import UndefinedTable
from psycopg.types.json import set_json_loads
from ska_oso_pdm.sb_definition import SBDefinition

from ska_db_oda.persistence.domain import CODEC
from ska_db_oda.persistence.unitofwork.abstractunitofwork import AbstractUnitOfWork
from ska_db_oda.persistence.unitofwork.filesystemunitofwork import FilesystemUnitOfWork
from ska_db_oda.persistence.unitofwork.postgresunitofwork import PostgresUnitOfWork
from tests.component import TEST_FILESYSTEM_IMPL


def get_uow(base_working_dir):
    return (
        FilesystemUnitOfWork(base_working_dir)
        if TEST_FILESYSTEM_IMPL
        else PostgresUnitOfWork()
    )


@pytest.fixture
def uow(base_working_dir):
    return get_uow(base_working_dir)


@pytest.fixture
def test_setup(uow):
    test_setup_cls = FilesystemTestSetup if TEST_FILESYSTEM_IMPL else PostgresTestSetup

    test_setup = test_setup_cls(uow)

    return test_setup


class TestSetup:
    uow: AbstractUnitOfWork

    def add_sbd(self, sbd: SBDefinition) -> None:
        raise NotImplementedError

    def get_all_sbd(self) -> List[SBDefinition]:
        raise NotImplementedError


class PostgresTestSetup(TestSetup):
    CREATE_TABLE_CMDS = {
        "tab_oda_sbd": """CREATE TABLE tab_oda_sbd
(id serial NOT NULL PRIMARY KEY,
info jsonb NOT NULL,
sbd_id text NOT NULL,
version integer NOT NULL,
created_by text NOT NULL,
created_on timestamp NOT NULL,
last_modified_on timestamp NOT NULL,
last_modified_by text NOT NULL);""",
        "tab_oda_sbi": """CREATE TABLE tab_oda_sbi
 (id serial NOT NULL PRIMARY KEY,
 sbd_id text NOT NULL,
 sbd_version serial NOT NULL,
 info jsonb NOT NULL,
 sbi_id text NOT NULL,
 version integer default 1,
 created_by text NOT NULL,
 created_on timestamp NOT NULL,
 last_modified_on timestamp NOT NULL,
 last_modified_by text NOT NULL
);""",
        "tab_oda_eb": """CREATE TABLE tab_oda_eb
 (id serial NOT NULL PRIMARY KEY,
 sbd_id text ,
 sbi_id text ,
 sbd_version integer ,
 eb_id text NOT NULL,
 info jsonb NOT NULL,
 version integer default 1,
 created_by text NOT NULL,
 created_on timestamp NOT NULL,
 last_modified_on timestamp NOT NULL,
 last_modified_by text NOT NULL );""",
        "tab_oda_prj": """CREATE TABLE tab_oda_prj
 (id serial NOT NULL PRIMARY KEY,
 prj_id text NOT NULL,
 info jsonb NOT NULL,
 version integer default 1,
 author text NOT NULL,
 created_by text NOT NULL,
 created_on timestamp NOT NULL,
 last_modified_on timestamp NOT NULL,
 last_modified_by text NOT NULL );""",
        "tab_oda_sbd_status_history": """CREATE TABLE tab_oda_sbd_status_history
 (id serial NOT NULL,
 sbd_ref text NOT NULL,
 sbd_version integer NOT NULL,
 previous_status sbd_status NOT NULL,
 current_status sbd_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, sbd_ref, version, current_status));""",
        "tab_oda_sbi_status_history": """CREATE TABLE tab_oda_sbi_status_history
 (id serial NOT NULL,
 sbi_ref text NOT NULL,
 sbi_version integer NOT NULL,
 previous_status sbi_status NOT NULL,
 current_status sbi_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, sbi_ref, version, current_status));""",
        "tab_oda_eb_status_history": """CREATE TABLE tab_oda_eb_status_history
 (id serial NOT NULL,
 eb_ref text NOT NULL,
 eb_version integer NOT NULL,
 previous_status eb_status NOT NULL,
 current_status eb_status NOT NULL,
 created_by text NOT NULL,
 created_on timestamp with time zone NOT NULL,
 last_modified_on timestamp with time zone NOT NULL,
 last_modified_by text NOT NULL,
 PRIMARY KEY (id, eb_ref, version, current_status));""",
        "tab_oda_prj_status_history": """CREATE TABLE tab_oda_prj_status_history
    (id serial NOT NULL,
    prj_ref text NOT NULL,
    prj_version integer NOT NULL,
    previous_status prj_status NOT NULL,
    current_status prj_status NOT NULL,
    created_by text NOT NULL,
    created_on timestamp with time zone NOT NULL,
    last_modified_on timestamp with time zone NOT NULL,
    last_modified_by text NOT NULL,
    PRIMARY KEY (id, prj_ref, version, current_status));""",
    }

    def __init__(self, uow: PostgresUnitOfWork):
        self.uow = uow
        for table_name in self.CREATE_TABLE_CMDS.keys():
            try:
                self.clear_all(table_name)
            except UndefinedTable:
                self.create_table(table_name)

    def clear_all(self, table_name: str) -> None:
        with self.uow._connection_pool.connection() as conn:
            conn.execute(
                sql.SQL("""DELETE FROM {table}""").format(
                    table=sql.Identifier(table_name),
                )
            )

    def create_table(self, table_name: str) -> None:
        with self.uow._connection_pool.connection() as conn:
            conn.execute(self.CREATE_TABLE_CMDS[table_name])

    def add_sbd(self, sbd: SBDefinition) -> None:
        with self.uow._connection_pool.connection() as conn:
            conn.execute(
                """INSERT INTO tab_oda_sbd
                    (id,info,sbd_id,version,created_on,created_by,last_modified_on,last_modified_by)
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s)""",
                (
                    10,
                    CODEC.dumps(sbd),
                    sbd.sbd_id,
                    sbd.metadata.version,
                    sbd.metadata.created_on,
                    sbd.metadata.created_by,
                    sbd.metadata.last_modified_on,
                    sbd.metadata.last_modified_by,
                ),
            )

    def get_all_sbd(self) -> List[SBDefinition]:
        with self.uow._connection_pool.connection() as conn:
            set_json_loads(lambda json_str: CODEC.loads(SBDefinition, json_str), conn)
            result = conn.execute("""SELECT * FROM tab_oda_sbd""").fetchall()
            return [row["info"] for row in result]
            # return [
            #     row["info"]
            #     if "info" in row.keys()
            #     else row
            #     for row in result
            # ]


class FilesystemTestSetup(TestSetup):
    def __init__(self, uow: FilesystemUnitOfWork):
        self.uow = uow

    def add_sbd(self, sbd: SBDefinition) -> None:
        path = self.uow._base_working_dir / "sbd" / sbd.sbd_id
        Path(path).mkdir(parents=True, exist_ok=True)

        entity_path = path / f"{sbd.metadata.version}.json"
        serialised_entity = CODEC.dumps(sbd)
        entity_path.write_text(serialised_entity)

    def get_all_sbd(self) -> List[SBDefinition]:
        return [
            CODEC.loads(SBDefinition, path.read_text())
            for path in (self.uow._base_working_dir / "sbd").rglob("*.json")
        ]
