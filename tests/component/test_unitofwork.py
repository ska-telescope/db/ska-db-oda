"""
Integration tests for the unit of work module.

The tests can be ran against either a postgres instance or the filesystem, depending on the TEST_FILESYSTEM_IMPL environment variable.
The tests run as part of k8s-test against a postgres instance, and k8s-test-filesystem using the filesystem.

The tests for the PostgresUnitOfWork require a running instance of Postgres, with connection details configured through
the environment variables. The KUBE_HOST and the Postgres related environment variables (shown in PostgresRepository) must be set.
"""
# test_setup fixture needs to be passed into several tests even though it is not called
# pylint: disable=unused-argument
import threading
import time
from datetime import datetime

import pytest

from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.persistence.domain.query import DateQuery, MatchType, UserQuery
from tests.component.conftest import get_uow
from tests.conftest import TestDataFactory


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-72087")
def test_repo_cant_be_access_outside_context_manager(uow):
    """
    Verify that the repository must be accessed via the context manager to ensure
    commit/rollback behaviour.
    """

    with pytest.raises(AttributeError):
        uow.sbds.add(TestDataFactory.sbdefinition())


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73506")
def test_add_with_rollback_leaves_repo_unchanged(sb_generator, uow, test_setup):
    """
    Verify that adding an SB to an uncommitted UoW session does not modify the
    repository state.
    """

    # This SB will be added but not committed.
    uncommitted_sb = next(sb_generator)

    with uow:
        # sanity checks for test setup_repository
        repo_len_before = len(test_setup.get_all_sbd())
        assert repo_len_before == 0, "pending transaction present for new session"

        uow.sbds.add(uncommitted_sb)
        # no session.commit() so transaction is rolled back

    tx_len_after = len(test_setup.get_all_sbd())
    assert tx_len_after == 0, "pending transactions not cleared after rollback"

    repo_len_after = len(test_setup.get_all_sbd())
    assert repo_len_after == repo_len_before, "repo size affected by uncommitted add"
    with pytest.raises(ODANotFound):
        with uow:
            uow.sbds.get(uncommitted_sb.sbd_id)


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73507")
def test_add_with_commit_modifies_repo(sb_generator, uow, test_setup):
    """
    Confirm that successfully committed additions affect the repository state
    but uncommitted additions leave the repository state unmodified.
    """
    # These SBs will be added and committed.
    committed_sbs = [next(sb_generator) for _ in range(3)]
    # These SBs will be added but not committed.
    uncommitted_sbs = [next(sb_generator) for _ in range(3)]

    with uow:
        repo_len_before = len(test_setup.get_all_sbd())
        for sb in committed_sbs:
            uow.sbds.add(sb)
        uow.commit()
        repo_len_committed = len(test_setup.get_all_sbd())

        for sb in uncommitted_sbs:
            uow.sbds.add(sb)
        # no session.commit() so these SBs remain uncommitted

    repo_len_after = len(test_setup.get_all_sbd())

    for sb in committed_sbs:
        assert sb.sbd_id in uow.sbds, "committed SB not added to repo"
    for sb in uncommitted_sbs:
        assert sb.sbd_id not in uow.sbds, "uncommitted SB added to repo"
    num_committed = repo_len_after - repo_len_before
    assert num_committed == len(
        committed_sbs
    ), "committed addition did not increase repo size"
    assert (
        repo_len_after == repo_len_committed
    ), "uncommitted transactions affected repo size"


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73508")
def test_rollback_uncommited_change_when_error(sb_generator, uow, test_setup):
    """
    Check that if an error occurs during a UnitOfWork then any uncommitted changes are not persisted
    """

    with pytest.raises(IOError):
        with uow:
            uow.sbds.add(next(sb_generator))
            uow.commit()
            uow.sbds.add(next(sb_generator))
            raise IOError

    assert len(test_setup.get_all_sbd()) == 1


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73509")
def test_sbd_put_new_version(uow, test_setup):
    """
    If a SDDefinition is committed through the UoW, the persisted metadata should have been updated
    """
    test_sbd_v1 = TestDataFactory.sbdefinition()

    test_setup.add_sbd(test_sbd_v1)

    with uow:
        uow.sbds.add(test_sbd_v1)
        uow.commit()

    result = test_setup.get_all_sbd()

    assert len(result) == 2
    assert set([sbd.metadata.version for sbd in result]) == {1, 2}


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73510")
def test_add_then_get_in_same_uow_returns_entity(uow, test_setup):
    """
    If an entity is added to the UoW and then a subsequent get is called, the entity should be
    returned as if the entity was persisted
    """

    test_sbd = TestDataFactory.sbdefinition()

    with uow:
        uow.sbds.add(test_sbd)
        result = uow.sbds.get(test_sbd.sbd_id)

    result.metadata = None
    test_sbd.metadata = None
    assert result == test_sbd


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73511")
def test_add_then_get_from_different_uow_doesnt_return_entity(
    uow, test_setup, base_working_dir
):
    """
    If an entity is added to one unit of work, it should not be retrievable
    from a different unit of work until it is committed.
    """

    test_sbd = TestDataFactory.sbdefinition()

    def blocking_uow(lock: threading.Lock):
        another_uow = get_uow(base_working_dir)
        with another_uow:
            another_uow.sbds.add(test_sbd)
            # The lock should already be acquired by the main thread, so the thread calling
            # this method will be blocked in a state where the UoW is still open and neither
            # a commit or rollback has happened.
            lock.acquire()
            another_uow.commit()

    # Start a UoW in a thread which will be blocked before commit/rollback
    test_lock = threading.Lock()
    test_lock.acquire()
    thread = threading.Thread(target=blocking_uow, args=(test_lock,))
    thread.start()

    # Assert the sbd is not retrieved
    with pytest.raises(ODANotFound):
        with uow:
            uow.sbds.get(test_sbd.sbd_id)

    test_lock.release()
    time.sleep(0.5)

    with uow:
        result = uow.sbds.get(test_sbd.sbd_id)

    # Ignore the metadata to avoid datetime comparisons
    result.metadata = None
    test_sbd.metadata = None
    assert result == test_sbd


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73512")
def test_adding_different_entity_types(uow, test_setup):
    """
    Adding multiple entities to a UoW and committing should persist them all.
    """

    with uow:
        uow.sbds.add(TestDataFactory.sbdefinition(sbd_id="sbd-123"))
        uow.sbis.add(TestDataFactory.sbinstance(sbi_id="sbi-123"))
        uow.ebs.add(TestDataFactory.executionblock(eb_id="eb-123"))
        uow.prjs.add(TestDataFactory.project(prj_id="prj-123"))
        uow.prsls.add(TestDataFactory.proposal(prsl_id="prsl-123"))
        uow.sbds_status_history.add(TestDataFactory.sbd_status(sbd_ref="sbd-123"))
        uow.sbis_status_history.add(TestDataFactory.sbi_status(sbi_ref="sbi-123"))
        uow.ebs_status_history.add(TestDataFactory.eb_status(eb_ref="eb-123"))
        uow.prjs_status_history.add(TestDataFactory.prj_status(prj_ref="prj-123"))
        uow.commit()

    with uow:
        assert "sbd-123" in uow.sbds
        assert "sbi-123" in uow.sbis
        assert "eb-123" in uow.ebs
        assert "prj-123" in uow.prjs
        assert "prsl-123" in uow.prsls
        assert "sbd-123" in uow.sbds_status_history
        assert "sbi-123" in uow.sbis_status_history
        assert "eb-123" in uow.ebs_status_history
        assert "prj-123" in uow.prjs_status_history


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73513")
def test_query_sbds(uow, test_setup):
    test_sbd = TestDataFactory.sbdefinition()

    test_setup.add_sbd(test_sbd)

    qry_params = DateQuery(
        query_type=DateQuery.QueryType.CREATED_BETWEEN,
        start=datetime.fromisoformat("1970-01-01T00:00:00.000000+00:00"),
    )

    with uow:
        result = uow.sbds.query(qry_params)

    assert result == [test_sbd]
    qry_params = DateQuery(
        query_type=DateQuery.QueryType.MODIFIED_BETWEEN,
        end=datetime.fromisoformat("2970-01-01T00:00:00.000000+00:00"),
    )
    with uow:
        result = uow.sbds.query(qry_params)

    assert result == [test_sbd]

    qry_params = UserQuery(user="TestUser", match_type=MatchType.STARTS_WITH)
    with uow:
        result = uow.sbds.query(qry_params)

    assert result == [test_sbd]


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73514")
def test_add_returns_fetched_id(uow, test_setup):
    """
    The SKUID call to fetch a new identifier is done in the repository layer.

    This tests that the `add` method of the repository returns the entity with
    an identifier populated, and checks this identifier exists in the ODA.
    """
    with uow:
        sbd = uow.sbds.add(TestDataFactory.sbdefinition(sbd_id=None))
        sbi = uow.sbis.add(TestDataFactory.sbinstance(sbi_id=None))
        eb = uow.ebs.add(TestDataFactory.executionblock(eb_id=None))
        prj = uow.prjs.add(TestDataFactory.project(prj_id=None))
        prsl = uow.prsls.add(TestDataFactory.proposal(prsl_id=None))
        uow.commit()

    with uow:
        assert sbd.sbd_id in uow.sbds
        assert sbi.sbi_id in uow.sbis
        assert eb.eb_id in uow.ebs
        assert prj.prj_id in uow.prjs
        assert prsl.prsl_id in uow.prsls


@pytest.mark.post_deployment
@pytest.mark.xray("XTP-73515")
def test_add_new_entity_without_metadata(uow, test_setup):
    """
    An new entity should be able to be passed to the ODA without supplying any metadata,
    as the ODA is responsible for populating the metadata upon creation
    """
    new_sbd = TestDataFactory.sbdefinition(sbd_id=None)
    new_sbd.metadata = None

    with uow:
        uow.sbds.add(new_sbd)
        uow.commit()

    result = test_setup.get_all_sbd()

    assert len(result) == 1
    assert result[0].metadata.version == 1
