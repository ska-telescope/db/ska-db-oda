EBS = "ebs"
SBDS = "sbds"
SBIS = "sbis"
PRJ = "prj"
PRSLS = "prsls"


ENTITY_CONSTANTS = {
    EBS: "eb_ref",
    SBDS: "sbd_ref",
    SBIS: "sbi_ref",
    PRJ: "prj_ref",
    PRSLS: "prsl_ref",
}

NOT_FOUND_MESSAGE = (
    lambda identifier: f"The requested identifier {identifier} could not be found."
)
