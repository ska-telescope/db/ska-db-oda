import logging
from typing import Annotated

from fastapi import APIRouter, Depends
from ska_oso_pdm import SBDefinition
from ska_oso_pdm.entity_status_history import SBDStatus, SBDStatusHistory

from ska_db_oda.persistence import oda
from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.rest.api import check_for_mismatch, get_qry_params, identifier_check
from ska_db_oda.rest.model import ApiQueryParameters, ApiStatusQueryParameters

LOGGER = logging.getLogger(__name__)

# Ideally would prefix this with sbds but the status entities do not follow the pattern
sbd_router = APIRouter()


@sbd_router.get(
    "/sbds/{sbd_id}",
    summary="Get an SBDefinition for a given identifier",
    response_model=SBDefinition,
)
def get_sbd(sbd_id: str) -> SBDefinition:
    """
    Function that a GET /sbds/<sbd_id> request is routed to.

    :param sbd_id: Requested identifier from the path parameter
    :return: The SBDefinition, if found in the ODA
    """
    with oda.uow() as uow:
        retrieved_sbd = uow.sbds.get(sbd_id)
    return retrieved_sbd


@sbd_router.get(
    "/sbds",
    summary="Get SBDefinitions matching the given query parameters",
    response_model=list[SBDefinition],
)
def get_sbds(query_params: ApiQueryParameters = Depends()) -> list[SBDefinition]:
    """
    Function that a GET /sbds request is routed to.

    :param query_params: Parameters to query the ODA by, provided as HTTP query parameters.
    :return: The SBDefinition wrapped in a Response, or appropriate error Response
    """
    query_param = get_qry_params(query_params)

    with oda.uow() as uow:
        sbds = uow.sbds.query(query_param)
    return sbds


@sbd_router.post(
    "/sbds",
    summary="Create a new SBDefinition",
    description=(
        "The entity to be created in the request body should not contain an sbd_id as"
        " fetching this from SKUID is the responsibility of the ODA. This endpoint will"
        " also create an initial status entity for the SBDefinition."
    ),
    response_model=SBDefinition,
)
def post_sbds(sbd: SBDefinition) -> SBDefinition:
    """
    Function that a POST /sbds request is routed to.

    :param sbd: SBDefinition to persist from the request body
    :return: The SBDefinition as it exists in the ODA
    """
    identifier_check(sbd)

    with oda.uow() as uow:
        persisted_sbd = uow.sbds.add(sbd)

        sbd_status_history = SBDStatusHistory(
            sbd_ref=persisted_sbd.sbd_id,
            sbd_version=persisted_sbd.metadata.version,
            current_status=SBDStatus.DRAFT,
            previous_status=SBDStatus.DRAFT,
        )

        uow.sbds_status_history.add(sbd_status_history)
        uow.commit()

    return persisted_sbd


@sbd_router.put(
    "/sbds/{sbd_id}",
    summary="Update an SBDefinition for a given identifier",
    description=(
        "The updated entity in the request body should contain an sbd_id that matches"
        " the path parameter."
    ),
    response_model=SBDefinition,
)
def put_sbd(sbd_id: str, sbd: SBDefinition) -> SBDefinition:
    """
    Function that a PUT /sbds/<sbd_id> request is routed to.

    :param sbd_id: Requested identifier from the path parameter
    :param sbd: SBDefinition to persist from the request body
    :return: The updated SBDefinition as it exists in the ODA
    """
    check_for_mismatch(sbd_id, sbd.sbd_id)

    with oda.uow() as uow:
        # Check the identifier already exists - after refactoring for BTN-3000
        # this check could be done within the add method
        if sbd_id not in uow.sbds:
            raise ODANotFound(identifier=sbd_id)

        persisted_sbd = uow.sbds.add(sbd)

        sbd_status_history = SBDStatusHistory(
            sbd_ref=persisted_sbd.sbd_id,
            sbd_version=persisted_sbd.metadata.version,
            current_status=SBDStatus.DRAFT,
            previous_status=SBDStatus.DRAFT,
        )

        uow.sbds_status_history.add(sbd_status_history)
        uow.commit()

    return persisted_sbd


@sbd_router.put(
    "/status/sbds/{sbd_id}",
    summary="Add a status entity linked to the SBDefinition for a given identifier",
    response_model=SBDStatusHistory,
)
def put_sbd_status(
    sbd_id: str, sbd_status_history: SBDStatusHistory
) -> SBDStatusHistory:
    """
    Function that a PUT /status/sbds/<sbd_id> request is routed to.

    :param sbd_id: The identifier of the SBDefinition the status is linked to
    :param sbd_status_history: SBDStatusHistory to persist from the request body
    :return: The SBDStatusHistory as it exists in the ODA
    """
    check_for_mismatch(sbd_id, sbd_status_history.sbd_ref)

    with oda.uow() as uow:
        if sbd_id not in uow.sbds:
            raise ODANotFound(identifier=sbd_id)

        persisted_sbd = uow.sbds_status_history.add(sbd_status_history)
        uow.commit()

    return persisted_sbd


@sbd_router.get(
    "/status/sbds/{sbd_id}",
    summary=(
        "Get the latest status entity linked to the SBDefinition for a given identifier"
    ),
    response_model=SBDStatusHistory,
)
def get_sbd_status(sbd_id: str, version: int = None) -> SBDStatusHistory:
    """
    Function that a GET /status/sbds/<sbd_id> request is routed to.
    This method is used to GET the current status for the given sbd_id

    :param sbd_id: The identifier of the SBDefinition the status is linked to
    :param version: Version of the SBDefinition to query, provided as HTTP query parameters
    :return: The current entity status, SBDStatusHistory
    """
    with oda.uow() as uow:
        retrieved_sbd_status = uow.sbds_status_history.get(
            sbd_id, version, is_status_history=False
        )
    return retrieved_sbd_status


@sbd_router.get(
    "/status/history/sbds",
    summary="Get the status history linked to the SBDefinition for a given identifier",
    description=(
        "If version parameter is not passed then the API retrieves the SBDefinitions"
        " status history for the latest available SBDefinitions version."
    ),
    response_model=list[SBDStatusHistory],
)
def get_sbd_status_history(
    query_params: Annotated[ApiStatusQueryParameters, Depends()]
) -> list[SBDStatusHistory]:
    """
    Function that a GET /status/sbds request is routed to.
    This method is used to GET status history for the given entity

    :param query_params: The HTTP query parameters, deserialised into an object
    :return: The status history, SBDStatusHistory wrapped in a Response, or appropriate error Response
    """
    query_params = get_qry_params(query_params)
    with oda.uow() as uow:
        sbd_status_history = uow.sbds_status_history.query(
            query_params, is_status_history=True
        )
    return sbd_status_history
