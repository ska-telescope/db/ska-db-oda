import logging

from fastapi import APIRouter, Depends
from ska_oso_pdm import Project
from ska_oso_pdm.entity_status_history import ProjectStatus, ProjectStatusHistory

from ska_db_oda.persistence import oda
from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.rest.api import check_for_mismatch, get_qry_params, identifier_check
from ska_db_oda.rest.model import ApiQueryParameters, ApiStatusQueryParameters

LOGGER = logging.getLogger(__name__)

# Ideally would prefix this with prjs but the status entities do not follow the pattern
prj_router = APIRouter()


@prj_router.get(
    "/prjs/{prj_id}",
    summary="Get a Project for a given identifier",
    response_model=Project,
)
def get_prj(prj_id: str) -> Project:
    """
    Function that a GET /prjs/<prj_id> request is routed to.

    :param prj_id: Requested identifier from the path parameter
    :return: The Project, if found in the ODA
    """
    with oda.uow() as uow:
        retrieved_prj = uow.prjs.get(prj_id)
    return retrieved_prj


@prj_router.get(
    "/prjs",
    summary="Get Projects matching the given query parameters",
    response_model=list[Project],
)
def get_prjs(query_params: ApiQueryParameters = Depends()) -> list[Project]:
    """
    Function that a GET /prjs request is routed to.

    :param query_params: Parameters to query the ODA by, provided as HTTP query parameters.
    :return: The Project wrapped in a Response, or appropriate error Response
    """
    query_param = get_qry_params(query_params)

    with oda.uow() as uow:
        prjs = uow.prjs.query(query_param)
    return prjs


@prj_router.post(
    "/prjs",
    summary="Create a new Project",
    description=(
        "The entity to be created in the request body should not contain a prj_id as"
        " fetching this from SKUID is the responsibility of the ODA. This endpoint will"
        " also create an initial status entity for the Project."
    ),
    response_model=Project,
)
def post_prjs(prj: Project) -> Project:
    """
    Function that a POST /prjs request is routed to.

    :param prj: Project to persist from the request body
    :return: The Project as it exists in the ODA
    """
    identifier_check(prj)

    with oda.uow() as uow:
        persisted_prj = uow.prjs.add(prj)

        prj_status_history = ProjectStatusHistory(
            prj_ref=persisted_prj.prj_id,
            prj_version=persisted_prj.metadata.version,
            current_status=ProjectStatus.DRAFT,
            previous_status=ProjectStatus.DRAFT,
        )

        uow.prjs_status_history.add(prj_status_history)
        uow.commit()

    return persisted_prj


@prj_router.put(
    "/prjs/{prj_id}",
    summary="Update a Project for a given identifier",
    description=(
        "The updated entity in the request body should contain a prj_id that matches"
        " the path parameter."
    ),
    response_model=Project,
)
def put_prj(prj_id: str, prj: Project) -> Project:
    """
    Function that a PUT /prjs/<prj_id> request is routed to.

    :param prj_id: Requested identifier from the path parameter
    :param prj: Project to persist from the request body
    :return: The updated Project as it exists in the ODA
    """
    check_for_mismatch(prj_id, prj.prj_id)

    with oda.uow() as uow:
        # Check the identifier already exists - after refactoring for BTN-3000
        # this check could be done within the add method
        if prj_id not in uow.prjs:
            raise ODANotFound(identifier=prj_id)

        persisted_prj = uow.prjs.add(prj)

        prj_status_history = ProjectStatusHistory(
            prj_ref=persisted_prj.prj_id,
            prj_version=persisted_prj.metadata.version,
            current_status=ProjectStatus.DRAFT,
            previous_status=ProjectStatus.DRAFT,
        )
        uow.prjs_status_history.add(prj_status_history)
        uow.commit()

    return persisted_prj


@prj_router.put(
    "/status/prjs/{prj_id}",
    summary="Add a status entity linked to the Project for a given identifier",
    response_model=ProjectStatusHistory,
)
def put_prj_status(
    prj_id: str, prj_status_history: ProjectStatusHistory
) -> ProjectStatusHistory:
    """
    Function that a PUT /status/prjs/<prj_id> request is routed to.

    :param prj_id: The identifier of the Project the status is linked to
    :param prj_status_history: ProjectStatusHistory to persist from the request body
    :return: The ProjectStatusHistory as it exists in the ODA
    """
    check_for_mismatch(prj_id, prj_status_history.prj_ref)

    with oda.uow() as uow:
        if prj_id not in uow.prjs:
            raise ODANotFound(identifier=prj_id)

        persisted_prj = uow.prjs_status_history.add(prj_status_history)
        uow.commit()

    return persisted_prj


@prj_router.get(
    "/status/prjs/{prj_id}",
    summary="Get the latest status entity linked to the Project for a given identifier",
    response_model=ProjectStatusHistory,
)
def get_prj_status(prj_id: str, version: int = None) -> ProjectStatusHistory:
    """
    Function that a GET /status/prjs/<prj_id> request is routed to.
    This method is used to GET the current status for the given prj_id

    :param prj_id: The identifier of the Project the status is linked to
    :param version: Version of the Project to query, provided as HTTP query parameters
    :return: The current entity status, ProjectStatusHistory
    """
    with oda.uow() as uow:
        retrieved_prj_status = uow.prjs_status_history.get(
            prj_id, version, is_status_history=False
        )
    return retrieved_prj_status


@prj_router.get(
    "/status/history/prjs",
    summary="Get the status history linked to the Project for a given identifier",
    description=(
        "If version parameter is not passed then the API retrieves the Projects status"
        " history for the latest available Projects version."
    ),
    response_model=list[ProjectStatusHistory],
)
def get_prj_status_history(
    query_params: ApiStatusQueryParameters = Depends(),
) -> list[ProjectStatusHistory]:
    """
    Function that a GET /status/prjs request is routed to.
    This method is used to GET status history for the given entity

    :param query_params: The HTTP query parameters, deserialised into an object
    :return: The status history, ProjectStatusHistory wrapped in a Response, or appropriate error Response
    """
    query_params = get_qry_params(query_params)
    with oda.uow() as uow:
        prj_status_history = uow.prjs_status_history.query(
            query_params, is_status_history=True
        )
    return prj_status_history
