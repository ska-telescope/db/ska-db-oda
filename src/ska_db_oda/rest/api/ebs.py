import logging
from typing import Union

from fastapi import APIRouter, Depends
from ska_oso_pdm import OSOExecutionBlock as ExecutionBlock
from ska_oso_pdm import SBDefinition, SBInstance
from ska_oso_pdm.entity_status_history import OSOEBStatus, OSOEBStatusHistory
from ska_oso_pdm.execution_block import RequestResponse

from ska_db_oda.constant import EBS
from ska_db_oda.persistence import oda
from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.rest.api import check_for_mismatch, get_qry_params, identifier_check
from ska_db_oda.rest.model import ApiQueryParameters, ApiStatusQueryParameters

LOGGER = logging.getLogger(__name__)

# Ideally would prefix this with ebs but the status entities do not follow the pattern
eb_router = APIRouter()


@eb_router.get(
    "/ebs/{eb_id}",
    summary="Get an ExecutionBlock for a given identifier",
    response_model=ExecutionBlock,
)
def get_eb(eb_id: str) -> ExecutionBlock:
    """
    Function that a GET /ebs/<eb_id> request is routed to.

    :param eb_id: Requested identifier from the path parameter
    :return: The ExecutionBlock, if found in the ODA
    """
    with oda.uow() as uow:
        retrieved_eb = uow.ebs.get(eb_id)
    return retrieved_eb


@eb_router.get(
    "/ebs",
    summary="Get ExecutionBlocks matching the given query parameters",
    response_model=list[ExecutionBlock],
)
def get_ebs(query_params: ApiQueryParameters = Depends()) -> list[ExecutionBlock]:
    """
    Function that a GET /ebs request is routed to.

    :param query_params: Parameters to query the ODA by, provided as HTTP query parameters.
    :return: The ExecutionBlock wrapped in a Response, or appropriate error Response
    """
    query_param = get_qry_params(query_params)

    with oda.uow() as uow:
        ebs = uow.ebs.query(query_param)
    return ebs


@eb_router.post(
    "/ebs",
    summary="Create a new ExecutionBlock",
    description=(
        "The entity to be created in the request body should not contain an eb_id as"
        " fetching this from SKUID is the responsibility of the ODA. This endpoint will"
        " also create an initial status entity for the ExecutionBlock."
    ),
    response_model=ExecutionBlock,
)
def post_ebs(eb: ExecutionBlock) -> ExecutionBlock:
    """
    Function that a POST /ebs request is routed to.

    :param eb: ExecutionBlock to persist from the request body
    :return: The ExecutionBlock as it exists in the ODA
    """
    identifier_check(eb)

    with oda.uow() as uow:
        persisted_eb = uow.ebs.add(eb)

        eb_status_history = OSOEBStatusHistory(
            eb_ref=persisted_eb.eb_id,
            eb_version=persisted_eb.metadata.version,
            current_status=OSOEBStatus.CREATED,
            previous_status=OSOEBStatus.CREATED,
        )

        uow.ebs_status_history.add(eb_status_history)
        uow.commit()

    return persisted_eb


@eb_router.put(
    "/ebs/{eb_id}",
    summary="Update an ExecutionBlock for a given identifier",
    description=(
        "The updated entity in the request body should contain an eb_id that matches"
        " the path parameter."
    ),
    response_model=ExecutionBlock,
)
def put_eb(eb_id: str, eb: ExecutionBlock) -> ExecutionBlock:
    """
    Function that a PUT /ebs/<eb_id> request is routed to.

    :param eb_id: Requested identifier from the path parameter
    :param eb: ExecutionBlock to persist from the request body
    :return: The updated ExecutionBlock as it exists in the ODA
    """
    check_for_mismatch(eb_id, eb.eb_id)

    with oda.uow() as uow:
        # Check the identifier already exists - after refactoring for BTN-3000
        # this check could be done within the add method
        if eb_id not in uow.ebs:
            raise ODANotFound(identifier=eb_id)
        persisted_eb = uow.ebs.add(eb)

        eb_status_history = OSOEBStatusHistory(
            eb_ref=persisted_eb.eb_id,
            eb_version=persisted_eb.metadata.version,
            current_status=OSOEBStatus.CREATED,
            previous_status=OSOEBStatus.CREATED,
        )
        uow.ebs_status_history.add(eb_status_history)
        uow.commit()

    return persisted_eb


@eb_router.put(
    "/status/ebs/{eb_id}",
    summary="Add a status entity linked to the ExecutionBlock for a given identifier",
    response_model=OSOEBStatusHistory,
)
def put_eb_status(
    eb_id: str, eb_status_history: OSOEBStatusHistory
) -> OSOEBStatusHistory:
    """
    Function that a PUT /status/ebs/<eb_id> request is routed to.

    :param eb_id: The identifier of the ExecutionBlock the status is linked to
    :param eb_status_history: OSOEBStatusHistory to persist from the request body
    :return: The OSOEBStatusHistory as it exists in the ODA
    """
    check_for_mismatch(eb_id, eb_status_history.eb_ref)

    with oda.uow() as uow:
        if eb_id not in uow.ebs:
            raise ODANotFound(identifier=eb_id)

        persisted_eb = uow.ebs_status_history.add(eb_status_history)
        uow.commit()

    return persisted_eb


@eb_router.get(
    "/status/ebs/{eb_id}",
    summary=(
        "Get the latest status entity linked to the ExecutionBlock for a given"
        " identifier"
    ),
    response_model=OSOEBStatusHistory,
)
def get_eb_status(eb_id: str, version: int = None) -> OSOEBStatusHistory:
    """
    Function that a GET /status/ebs/<eb_id> request is routed to.
    This method is used to GET the current status for the given eb_id

    :param eb_id: The identifier of the ExecutionBlock the status is linked to
    :param version: Version of the ExecutionBlock to query, provided as HTTP query parameters
    :return: The current entity status, OSOEBStatusHistory
    """
    with oda.uow() as uow:
        retrieved_eb_status = uow.ebs_status_history.get(
            eb_id, version, is_status_history=False
        )
    return retrieved_eb_status


@eb_router.get(
    "/status/history/ebs",
    summary=(
        "Get the status history linked to the ExecutionBlock for a given identifier"
    ),
    description=(
        "If version parameter is not passed then the API retrieves the ExecutionBlocks"
        " status history for the latest available ExecutionBlocks version."
    ),
    response_model=list[OSOEBStatusHistory],
)
def get_eb_status_history(
    query_params: ApiStatusQueryParameters = Depends(),
) -> list[OSOEBStatusHistory]:
    """
    Function that a GET /status/ebs request is routed to.
    This method is used to GET status history for the given entity

    :param query_params: The HTTP query parameters, deserialised into an object
    :return: The status history, OSOEBStatusHistory wrapped in a Response, or appropriate error Response
    """
    query_params = get_qry_params(query_params)
    with oda.uow() as uow:
        eb_status_history = uow.ebs_status_history.query(
            query_params, is_status_history=True
        )
    return eb_status_history


@eb_router.get(
    "/ebs/{eb_id}/{associated_entity}",
    summary="Get the Execution Block and the entity ",
    description=(
        "If version parameter is not passed then the API retrieves the ExecutionBlocks"
        " status history for the latest available ExecutionBlocks version."
    ),
    response_model=Union[SBDefinition, SBInstance],
)
def get_eb_with_associated_entities(
    eb_id: str, associated_entity: str
) -> Union[SBDefinition, SBInstance]:
    """
    Function that a GET /ebs/<eb_id>/<associated_entity> request is routed to.

    :param eb_id: Requested identifier from the get parameter
    :param associated_entity: Associated entity to fetch data.
    :return: The relationship entity wrapped in a Response, or appropriate error Response
    """
    with oda.uow() as uow:
        parent_entity = EBS
        retrieved_eb = uow.entity_relationship.get_relationship(
            eb_id, parent_entity, associated_entity
        )
    return retrieved_eb


@eb_router.patch(
    "/ebs/{eb_id}/request_response",
    summary="Get the Execution Block and the entity ",
    description=(
        "If version parameter is not passed then the API retrieves the ExecutionBlocks"
        " status history for the latest available ExecutionBlocks version."
    ),
    response_model=ExecutionBlock,
)
def add_request_response(
    eb_id: str, request_response: RequestResponse
) -> ExecutionBlock:
    """
    Function that a PATCH /ebs/<eb_id>/request_response request is routed to.

    :param eb_id: The identifier of the ExecutionBlock to add the RequestResponse to
    :param request_response: RequestResponse to add to the ExecutionBlock
    :return: The ExecutionBlock as it exists in the ODA
    """
    with oda.uow() as uow:
        eb = uow.ebs.get(eb_id)
        if eb.request_responses is None:
            eb.request_responses = [request_response]
        else:
            eb.request_responses.append(request_response)
        persisted_eb = uow.ebs.add(eb)
        uow.commit()

    return persisted_eb
