import logging

from fastapi import APIRouter, Depends
from ska_oso_pdm import Proposal

from ska_db_oda.persistence import oda
from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.rest.api import check_for_mismatch, get_qry_params, identifier_check
from ska_db_oda.rest.model import ApiQueryParameters

LOGGER = logging.getLogger(__name__)

# Ideally would prefix this with prsls but the status entities do not follow the pattern
prsl_router = APIRouter()


@prsl_router.get(
    "/prsls/{prsl_id}",
    summary="Get a Proposal for a given identifier",
    response_model=Proposal,
)
def get_prsl(prsl_id: str) -> Proposal:
    """
    Function that a GET /prsls/<prsl_id> request is routed to.

    :param prsl_id: Requested identifier from the path parameter
    :return: The Proposal, if found in the ODA
    """
    with oda.uow() as uow:
        retrieved_prsl = uow.prsls.get(prsl_id)
    return retrieved_prsl


@prsl_router.get(
    "/prsls",
    summary="Get Proposals matching the given query parameters",
    response_model=list[Proposal],
)
def get_prsls(query_params: ApiQueryParameters = Depends()) -> list[Proposal]:
    """
    Function that a GET /prsls request is routed to.

    :param query_params: Parameters to query the ODA by, provided as HTTP query parameters.
    :return: The Proposal wrapped in a Response, or appropriate error Response
    """
    query_param = get_qry_params(query_params)

    with oda.uow() as uow:
        prsls = uow.prsls.query(query_param)
    return prsls


@prsl_router.post(
    "/prsls",
    summary="Create a new Proposal",
    description=(
        "The entity to be created in the request body should not contain a prsl_id as"
        " fetching this from SKUID is the responsibility of the ODA."
    ),
    response_model=Proposal,
)
def post_prsls(prsl: Proposal) -> Proposal:
    """
    Function that a POST /prsls request is routed to.

    :param prsl: Proposal to persist from the request body
    :return: The Proposal as it exists in the ODA
    """
    identifier_check(prsl)

    with oda.uow() as uow:
        persisted_prsl = uow.prsls.add(prsl)
        uow.commit()

    return persisted_prsl


@prsl_router.put(
    "/prsls/{prsl_id}",
    summary="Update a Proposal for a given identifier",
    description=(
        "The updated entity in the request body should contain a prsl_id that matches"
        " the path parameter."
    ),
    response_model=Proposal,
)
def put_prsl(prsl_id: str, prsl: Proposal) -> Proposal:
    """
    Function that a PUT /prsls/<prsl_id> request is routed to.

    :param prsl_id: Requested identifier from the path parameter
    :param prsl: Proposal to persist from the request body
    :return: The updated Proposal as it exists in the ODA
    """
    check_for_mismatch(prsl_id, prsl.prsl_id)

    with oda.uow() as uow:
        # Check the identifier already exists - after refactoring for BTN-3000
        # this check could be done within the add method
        if prsl_id not in uow.prsls:
            raise ODANotFound(identifier=prsl_id)

        persisted_prsl = uow.prsls.add(prsl)
        uow.commit()

    return persisted_prsl
