import logging

from fastapi import APIRouter, Depends
from ska_oso_pdm import SBInstance
from ska_oso_pdm.entity_status_history import SBIStatus, SBIStatusHistory

from ska_db_oda.persistence import oda
from ska_db_oda.persistence.domain.errors import ODANotFound
from ska_db_oda.rest.api import check_for_mismatch, get_qry_params, identifier_check
from ska_db_oda.rest.model import ApiQueryParameters, ApiStatusQueryParameters

LOGGER = logging.getLogger(__name__)

# Ideally would prefix this with sbis but the status entities do not follow the pattern
sbi_router = APIRouter()


@sbi_router.get(
    "/sbis/{sbi_id}",
    summary="Get an SBInstance for a given identifier",
    response_model=SBInstance,
)
def get_sbi(sbi_id: str) -> SBInstance:
    """
    Function that a GET /sbis/<sbi_id> request is routed to.

    :param sbi_id: Requested identifier from the path parameter
    :return: The SBInstance, if found in the ODA
    """
    with oda.uow() as uow:
        retrieved_sbi = uow.sbis.get(sbi_id)
    return retrieved_sbi


@sbi_router.get(
    "/sbis",
    summary="Get SBInstances matching the given query parameters",
    response_model=list[SBInstance],
)
def get_sbis(query_params: ApiQueryParameters = Depends()) -> list[SBInstance]:
    """
    Function that a GET /sbis request is routed to.

    :param query_params: Parameters to query the ODA by, provided as HTTP query parameters.
    :return: The SBInstance wrapped in a Response, or appropriate error Response
    """
    query_param = get_qry_params(query_params)

    with oda.uow() as uow:
        sbis = uow.sbis.query(query_param)
    return sbis


@sbi_router.post(
    "/sbis",
    summary="Create a new SBInstance",
    description=(
        "The entity to be created in the request body should not contain an sbi_id as"
        " fetching this from SKUID is the responsibility of the ODA. This endpoint will"
        " also create an initial status entity for the SBInstance."
    ),
    response_model=SBInstance,
)
def post_sbis(sbi: SBInstance) -> SBInstance:
    """
    Function that a POST /sbis request is routed to.

    :param sbi: SBInstance to persist from the request body
    :return: The SBInstance as it exists in the ODA
    """
    identifier_check(sbi)

    with oda.uow() as uow:
        persisted_sbi = uow.sbis.add(sbi)

        sbi_status_history = SBIStatusHistory(
            sbi_ref=persisted_sbi.sbi_id,
            sbi_version=persisted_sbi.metadata.version,
            current_status=SBIStatus.CREATED,
            previous_status=SBIStatus.CREATED,
        )

        uow.sbis_status_history.add(sbi_status_history)
        uow.commit()

    return persisted_sbi


@sbi_router.put(
    "/sbis/{sbi_id}",
    summary="Update an SBInstance for a given identifier",
    description=(
        "The updated entity in the request body should contain an sbi_id that matches"
        " the path parameter."
    ),
    response_model=SBInstance,
)
def put_sbi(sbi_id: str, sbi: SBInstance) -> SBInstance:
    """
    Function that a PUT /sbis/<sbi_id> request is routed to.

    :param sbi_id: Requested identifier from the path parameter
    :param sbi: SBInstance to persist from the request body
    :return: The updated SBInstance as it exists in the ODA
    """
    check_for_mismatch(sbi_id, sbi.sbi_id)

    with oda.uow() as uow:
        # Check the identifier already exists - after refactoring for BTN-3000
        # this check could be done within the add method
        if sbi_id not in uow.sbis:
            raise ODANotFound(identifier=sbi_id)

        persisted_sbi = uow.sbis.add(sbi)

        sbi_status_history = SBIStatusHistory(
            sbi_ref=persisted_sbi.sbi_id,
            sbi_version=persisted_sbi.metadata.version,
            current_status=SBIStatus.CREATED,
            previous_status=SBIStatus.CREATED,
        )
        uow.sbis_status_history.add(sbi_status_history)
        uow.commit()

    return persisted_sbi


@sbi_router.put(
    "/status/sbis/{sbi_id}",
    summary="Add a status entity linked to the SBInstance for a given identifier",
    response_model=SBIStatusHistory,
)
def put_sbi_status(
    sbi_id: str, sbi_status_history: SBIStatusHistory
) -> SBIStatusHistory:
    """
    Function that a PUT /status/sbis/<sbi_id> request is routed to.

    :param sbi_id: The identifier of the SBInstance the status is linked to
    :param sbi_status_history: SBIStatusHistory to persist from the request body
    :return: The SBIStatusHistory as it exists in the ODA
    """
    check_for_mismatch(sbi_id, sbi_status_history.sbi_ref)

    with oda.uow() as uow:
        if sbi_id not in uow.sbis:
            raise ODANotFound(identifier=sbi_id)

        persisted_sbi = uow.sbis_status_history.add(sbi_status_history)
        uow.commit()

    return persisted_sbi


@sbi_router.get(
    "/status/sbis/{sbi_id}",
    summary=(
        "Get the latest status entity linked to the SBInstance for a given identifier"
    ),
    response_model=SBIStatusHistory,
)
def get_sbi_status(sbi_id: str, version: int = None) -> SBIStatusHistory:
    """
    Function that a GET /status/sbis/<sbi_id> request is routed to.
    This method is used to GET the current status for the given sbi_id

    :param sbi_id: The identifier of the SBInstance the status is linked to
    :param version: Version of the SBInstance to query, provided as HTTP query parameters
    :return: The current entity status, SBIStatusHistory
    """
    with oda.uow() as uow:
        retrieved_sbi_status = uow.sbis_status_history.get(
            sbi_id, version, is_status_history=False
        )
    return retrieved_sbi_status


@sbi_router.get(
    "/status/history/sbis",
    summary="Get the status history linked to the SBInstance for a given identifier",
    description=(
        "If version parameter is not passed then the API retrieves the SBInstances"
        " status history for the latest available SBInstances version."
    ),
    response_model=list[SBIStatusHistory],
)
def get_sbi_status_history(
    query_params: ApiStatusQueryParameters = Depends(),
) -> list[SBIStatusHistory]:
    """
    Function that a GET /status/sbis request is routed to.
    This method is used to GET status history for the given entity

    :param query_params: The HTTP query parameters, deserialised into an object
    :return: The status history, SBIStatusHistory wrapped in a Response, or appropriate error Response
    """
    query_params = get_qry_params(query_params)
    with oda.uow() as uow:
        sbi_status_history = uow.sbis_status_history.query(
            query_params, is_status_history=True
        )
    return sbi_status_history
