"""
This package implements SKA OSO Data Archive
"""
from os import getenv

from ska_ser_logging import configure_logging

LOG_LEVEL = getenv("LOG_LEVEL", "INFO").upper()

configure_logging(LOG_LEVEL)
