import logging
from os import getenv
from typing import Union

from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance
from ska_oso_pdm.entity_status_history import (
    EntityStatus,
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)
from ska_oso_pdm.openapi import OpenAPICodec
from ska_ser_skuid.client import SkuidClient

from ska_db_oda.persistence.domain.errors import EntityNotSupported

LOGGER = logging.getLogger(__name__)

OSOEntity = Union[
    SBDefinition,
    SBInstance,
    OSOExecutionBlock,
    Project,
    Proposal,
    OSOEBStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
    ProjectStatusHistory,
]

SKUID_URL = getenv("SKUID_URL", "http://ska-ser-skuid-test-svc:9870")

skuid = SkuidClient(SKUID_URL)


CODEC = OpenAPICodec()


def is_entity_status_history(entity: OSOEntity):
    if isinstance(
        entity, (SBDefinition, SBInstance, OSOExecutionBlock, Project, Proposal)
    ):
        return False
    if isinstance(
        entity,
        EntityStatus,
    ):
        return True
    raise EntityNotSupported(entity)


def get_identifier(entity: OSOEntity):
    """
    Retrieves the unique identifier of the entity depending on its type
    """
    match entity:
        case SBDefinition():
            return entity.sbd_id
        case SBInstance():
            return entity.sbi_id
        case OSOExecutionBlock():
            return entity.eb_id
        case Project():
            return entity.prj_id
        case Proposal():
            return entity.prsl_id
        case OSOEBStatusHistory():
            return entity.eb_ref
        case SBDStatusHistory():
            return entity.sbd_ref
        case SBIStatusHistory():
            return entity.sbi_ref
        case ProjectStatusHistory():
            return entity.prj_ref

        case _:
            raise EntityNotSupported(entity)


def set_identifier(entity: OSOEntity, entity_id: str):
    """
    Sets the entity_id on the correct field of the entity. Eg for an SBDefinition sbd
    this function will set sbd.sbd_id = entity_id
    """
    match entity:
        case SBDefinition():
            identifier_attr = "sbd_id"
        case SBInstance():
            identifier_attr = "sbi_id"
        case OSOExecutionBlock():
            identifier_attr = "eb_id"
        case Project():
            identifier_attr = "prj_id"
        case Proposal():
            identifier_attr = "prsl_id"
        case OSOEBStatusHistory():
            identifier_attr = "eb_ref"
        case SBDStatusHistory():
            identifier_attr = "sbd_ref"
        case SBIStatusHistory():
            identifier_attr = "sbi_ref"
        case ProjectStatusHistory():
            identifier_attr = "prj_ref"
        case _:
            raise EntityNotSupported(entity)
    setattr(entity, identifier_attr, entity_id)


def get_identifier_or_fetch_from_skuid(entity: OSOEntity):
    """
    Get the identifier field of the entity, or if one isn't present then fetches one from skuid.

    Note: this has a side effect of setting the identifier on the passed object to the one fetched from skuid

    :param entity: the entity to return/fetch the identifier of
    :return: the identifier of the entity as it was passed, or the new one from skuid
    """
    if (entity_id := get_identifier(entity)) is None:
        entity_id = fetch_id_from_skuid(entity)
        set_identifier(entity, entity_id)
    return entity_id


def fetch_id_from_skuid(entity: OSOEntity) -> str:
    """
    Get the identifier field of the entity, fetches one from skuid.

    :param entity: the entity to return/fetch the identifier of
    :return: the identifier of the entity from skuid
    :raises: Value Error for unknown entity
    """
    match entity:
        case SBDefinition() | SBDStatusHistory():
            skuid_entity_type = "sbd"
        case SBInstance() | SBIStatusHistory():
            skuid_entity_type = "sbi"
        case OSOExecutionBlock() | OSOEBStatusHistory():
            skuid_entity_type = "eb"
        case Project() | ProjectStatusHistory():
            skuid_entity_type = "prj"
        case Proposal():
            skuid_entity_type = "prsl"
        case _:
            raise EntityNotSupported(entity)
    LOGGER.debug("Fetching identifier from skuid for type %s", skuid_entity_type)
    return skuid.fetch_skuid(skuid_entity_type)
