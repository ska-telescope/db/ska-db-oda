# pylint: disable=E1101
import json
from abc import ABC, abstractmethod
from typing import Dict, Generic, TypeVar

from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance
from ska_oso_pdm.entity_status_history import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)

from ska_db_oda.constant import EBS, SBDS, SBIS
from ska_db_oda.persistence.infrastructure.postgres.sqlqueries import (
    TableDetails,
    result_to_entity_relationship_metadata,
    result_to_metadata,
)

T = TypeVar("T")


class PostgresMapping(Generic[T]):
    """
    A class which provides functionality for mapping between entities stored in the ODA
    and the PostgreSQL tables and types.
    """

    @property
    @abstractmethod
    def table_details(self) -> TableDetails:
        """
        This property encapsulates the information about the database table and how it maps to the entity,
        eg the table name, primary key column and mappings from the entity to columns.
        """
        raise NotImplementedError

    @abstractmethod
    def jsonb_load(self, json_str: str) -> T:
        """
        Used to deserialise the Postgres jsonb type to a Python object

        It is set on connections where the jsonb is queried.
        """
        raise NotImplementedError

    @abstractmethod
    def result_to_entity(self, query_result: Dict) -> T:
        """
        Coverts the dict of table columns into the entity.
        """
        raise NotImplementedError


class EntityRelationshipPostgresMapping(ABC):
    """
    Base abstract class for managing relationships between different entities.
    """

    def get_mapping(self, entity_type: str) -> TableDetails:
        """
        Returns the mapping instance for the given entity type.
        """
        raise NotImplementedError

    def result_to_entity_relationship(
        self, query_result: Dict, parent_entity: str, associated_entity: str
    ) -> T:
        """
        Converts the dict of table columns into the entity.
        """
        raise NotImplementedError


class SBDefinitionMapping(PostgresMapping[SBDefinition]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_sbd",
            identifier_field="sbd_id",
            column_map={
                "info": lambda sbd: sbd.model_dump_json(),
                "sbd_id": lambda sbd: sbd.sbd_id,
            },
        )

    def jsonb_load(self, json_str: str) -> SBDefinition:
        return SBDefinition.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> SBDefinition:
        sbd = query_result["info"]
        sbd.metadata = result_to_metadata(query_result)
        return sbd


class ExecutionBlockMapping(PostgresMapping[OSOExecutionBlock]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_eb",
            identifier_field="eb_id",
            column_map={
                "info": lambda eb: eb.model_dump_json(),
                "eb_id": lambda eb: eb.eb_id,
                "sbd_id": lambda eb: eb.sbd_ref,
                "sbi_id": lambda eb: eb.sbi_ref,
                "sbd_version": lambda eb: eb.sbd_version,
            },
        )

    def jsonb_load(self, json_str: str) -> OSOExecutionBlock:
        """
        :param: json_str: binary entity data
        Method to decode binary value to Entity object
        """
        return OSOExecutionBlock.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> OSOExecutionBlock:
        eb = query_result["info"]
        eb.metadata = result_to_metadata(query_result)
        return eb


class ProjectMapping(PostgresMapping[Project]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_prj",
            identifier_field="prj_id",
            column_map={
                "info": lambda prj: prj.model_dump_json(),
                "prj_id": lambda prj: prj.prj_id,
                "author": lambda prj: ", ".join(prj.author.pis),
            },
        )

    def jsonb_load(self, json_str: str) -> Project:
        return Project.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> Project:
        prj = query_result["info"]
        prj.metadata = result_to_metadata(query_result)
        return prj


class ProposalMapping(PostgresMapping[Proposal]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_prsl",
            identifier_field="prsl_id",
            column_map={
                "info": lambda prsl: prsl.model_dump_json(),
                "prsl_id": lambda prsl: prsl.prsl_id,
                "status": lambda prsl: prsl.status,
                "cycle": lambda prsl: prsl.cycle,
                "submitted_on": lambda prsl: prsl.submitted_on,
                "submitted_by": lambda prsl: prsl.submitted_by,
                "investigators": lambda prsl: prsl.investigator_refs,
            },
        )

    def jsonb_load(self, json_str: str) -> Proposal:
        return Proposal.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> Proposal:  # pylint: disable=W0613
        prsl = query_result["info"]
        prsl.metadata = result_to_metadata(query_result)
        return prsl


class SBInstanceMapping(PostgresMapping[SBInstance]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_sbi",
            identifier_field="sbi_id",
            column_map={
                "info": lambda sbi: sbi.model_dump_json(),
                "sbi_id": lambda sbi: sbi.sbi_id,
                "sbd_id": lambda sbi: sbi.sbd_ref,
                "sbd_version": lambda sbi: sbi.sbd_version,
            },
        )

    def jsonb_load(self, json_str: str) -> SBInstance:
        return SBInstance.model_validate_json(json_str)

    def result_to_entity(
        self, query_result: Dict  # pylint: disable=W0613
    ) -> SBInstance:
        sbi = query_result["info"]
        sbi.metadata = result_to_metadata(query_result)
        return sbi


class EntityRelationshipMapping(EntityRelationshipPostgresMapping):
    def __init__(self):
        self.sbds = SBDefinitionMapping()
        self.sbis = SBInstanceMapping()
        self.ebs = ExecutionBlockMapping()
        self.prjs = ProjectMapping()
        self.prsls = ProposalMapping()

    def get_mapping(self, entity_type: str) -> TableDetails:
        """
        Returns the mapping instance for the given entity type.
        """
        entities_table_details = {
            EBS: self.ebs.table_details,
            SBIS: self.sbis.table_details,
            SBDS: self.sbds.table_details,
        }
        return entities_table_details.get(entity_type)

    def result_to_entity_relationship(
        self, query_result: Dict, parent_entity: str, associated_entity: str
    ) -> list:
        """
        Convert jsonified result into Entity object and modified metadata based
        outer metadata keys like created by, version etc.
        :param parent_entity: primary entity table for join columns.
        :param associated_entity: secondary entity table for join columns.
        """
        if parent_entity == EBS:
            eb_block = self.ebs.jsonb_load(json.dumps(query_result["info"]))
            eb_block.metadata = result_to_metadata(query_result)
        if associated_entity == SBIS:
            sbi_block = self.sbis.jsonb_load(
                json.dumps(query_result["tab_oda_sbi_info"])
            )
            sbi_block.metadata = result_to_entity_relationship_metadata(
                query_result, self.sbis.table_details.table_name
            )
            return sbi_block
        if associated_entity == SBDS:
            sbd_block = self.sbds.jsonb_load(
                json.dumps(query_result["tab_oda_sbd_info"])
            )
            sbd_block.metadata = result_to_entity_relationship_metadata(
                query_result, self.sbds.table_details.table_name
            )
            return sbd_block


class SBDefinitionStatusHistoryMapping(PostgresMapping[SBDStatusHistory]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_sbd_status_history",
            identifier_field="sbd_ref",
            column_map={
                "sbd_ref": lambda sbd: sbd.sbd_ref,
                "sbd_version": lambda sbd: sbd.sbd_version,
                "previous_status": lambda sbd: sbd.previous_status,
                "current_status": lambda sbd: sbd.current_status,
            },
            metadata_map={
                "created_on": lambda entity: entity.metadata.created_on,
                "created_by": lambda entity: entity.metadata.created_by,
                "last_modified_on": lambda entity: entity.metadata.last_modified_on,
                "last_modified_by": lambda entity: entity.metadata.last_modified_by,
            },
        )

    def jsonb_load(self, json_str: str) -> SBDStatusHistory:
        return SBDStatusHistory.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> SBDStatusHistory:
        sbd_status_history = SBDStatusHistory(
            sbd_ref=query_result["sbd_ref"],
            sbd_version=query_result["sbd_version"],
            previous_status=query_result["previous_status"],
            current_status=query_result["current_status"],
            metadata=result_to_metadata(query_result),
        )

        return sbd_status_history


class OSOExecutionBlockStatusHistoryMapping(PostgresMapping[OSOEBStatusHistory]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_eb_status_history",
            identifier_field="eb_ref",
            column_map={
                "eb_ref": lambda eb: eb.eb_ref,
                "eb_version": lambda eb: eb.eb_version,
                "previous_status": lambda eb: eb.previous_status,
                "current_status": lambda eb: eb.current_status,
            },
            metadata_map={
                "created_on": lambda entity: entity.metadata.created_on,
                "created_by": lambda entity: entity.metadata.created_by,
                "last_modified_on": lambda entity: entity.metadata.last_modified_on,
                "last_modified_by": lambda entity: entity.metadata.last_modified_by,
            },
        )

    def jsonb_load(self, json_str: str) -> OSOEBStatusHistory:
        return OSOEBStatusHistory.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> OSOEBStatusHistory:
        eb_status_history = OSOEBStatusHistory(
            eb_ref=query_result["eb_ref"],
            eb_version=query_result["eb_version"],
            previous_status=query_result["previous_status"],
            current_status=query_result["current_status"],
            metadata=result_to_metadata(query_result),
        )

        return eb_status_history


class SBInstanceStatusHistoryMapping(PostgresMapping[SBIStatusHistory]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_sbi_status_history",
            identifier_field="sbi_ref",
            column_map={
                "sbi_ref": lambda sbi: sbi.sbi_ref,
                "sbi_version": lambda sbi: sbi.sbi_version,
                "previous_status": lambda sbi: sbi.previous_status,
                "current_status": lambda sbi: sbi.current_status,
            },
            metadata_map={
                "created_on": lambda entity: entity.metadata.created_on,
                "created_by": lambda entity: entity.metadata.created_by,
                "last_modified_on": lambda entity: entity.metadata.last_modified_on,
                "last_modified_by": lambda entity: entity.metadata.last_modified_by,
            },
        )

    def jsonb_load(self, json_str: str) -> SBIStatusHistory:
        return SBIStatusHistory.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> SBIStatusHistory:
        sbi_status_history = SBIStatusHistory(
            sbi_ref=query_result["sbi_ref"],
            sbi_version=query_result["sbi_version"],
            previous_status=query_result["previous_status"],
            current_status=query_result["current_status"],
            metadata=result_to_metadata(query_result),
        )

        return sbi_status_history


class ProjectStatusHistoryMapping(PostgresMapping[ProjectStatusHistory]):
    @property
    def table_details(self) -> TableDetails:
        return TableDetails(
            table_name="tab_oda_prj_status_history",
            identifier_field="prj_ref",
            column_map={
                "prj_ref": lambda prj: prj.prj_ref,
                "prj_version": lambda prj: prj.prj_version,
                "previous_status": lambda prj: prj.previous_status,
                "current_status": lambda prj: prj.current_status,
            },
            metadata_map={
                "created_on": lambda entity: entity.metadata.created_on,
                "created_by": lambda entity: entity.metadata.created_by,
                "last_modified_on": lambda entity: entity.metadata.last_modified_on,
                "last_modified_by": lambda entity: entity.metadata.last_modified_by,
            },
        )

    def jsonb_load(self, json_str: str) -> ProjectStatusHistory:
        return ProjectStatusHistory.model_validate_json(json_str)

    def result_to_entity(self, query_result: Dict) -> ProjectStatusHistory:
        prj_status_history = ProjectStatusHistory(
            prj_ref=query_result["prj_ref"],
            prj_version=query_result["prj_version"],
            previous_status=query_result["previous_status"],
            current_status=query_result["current_status"],
            metadata=result_to_metadata(query_result),
        )

        return prj_status_history
