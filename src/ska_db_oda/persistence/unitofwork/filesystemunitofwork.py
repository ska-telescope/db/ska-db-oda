"""
FilesystemUnitOfWork adds unit of work transaction support to the FilesystemRepository.
"""
# We mimic the database commit/rollback by accessing the transactions inside the repository
# pylint: disable=protected-access
import logging
from os import PathLike
from pathlib import Path
from typing import Union

from ska_db_oda.persistence.domain.repository import (
    EntityRelationshipRepository,
    ExecutionBlockRepository,
    ExecutionBlockStatusHistoryRepository,
    ProjectRepository,
    ProjectStatusHistoryRepository,
    ProposalRepository,
    SBDefinitionRepository,
    SBDefinitionStatusHistoryRepository,
    SBInstanceRepository,
    SBInstanceStatusHistoryRepository,
)
from ska_db_oda.persistence.infrastructure.filesystem.mapping import (
    EntityRelationshipMapping,
    ExecutionBlockMapping,
    OSOExecutionBlockStatusHistoryMapping,
    ProjectMapping,
    ProjectStatusHistoryMapping,
    ProposalMapping,
    SBDefinitionMapping,
    SBDefinitionStatusHistoryMapping,
    SBInstanceMapping,
    SBInstanceStatusHistoryMapping,
)
from ska_db_oda.persistence.infrastructure.filesystem.repository import FilesystemBridge
from ska_db_oda.persistence.unitofwork.abstractunitofwork import AbstractUnitOfWork

LOGGER = logging.getLogger(__name__)


class FilesystemUnitOfWork(AbstractUnitOfWork):
    """
    Implementation of the AbstractUnitOfWork which persists the data in the filesystem
    """

    def __init__(self, base_working_dir: Union[str, PathLike] = Path("/var/lib/oda")):
        self._base_working_dir = base_working_dir

    def __enter__(self):
        self.sbds = SBDefinitionRepository(
            FilesystemBridge(SBDefinitionMapping(), self._base_working_dir)
        )
        self.sbis = SBInstanceRepository(
            FilesystemBridge(SBInstanceMapping(), self._base_working_dir)
        )
        self.ebs = ExecutionBlockRepository(
            FilesystemBridge(ExecutionBlockMapping(), self._base_working_dir)
        )
        self.prjs = ProjectRepository(
            FilesystemBridge(ProjectMapping(), self._base_working_dir)
        )
        self.prsls = ProposalRepository(
            FilesystemBridge(ProposalMapping(), self._base_working_dir)
        )
        self.entity_relationship = EntityRelationshipRepository(
            FilesystemBridge(EntityRelationshipMapping(), self._base_working_dir)
        )
        self.sbds_status_history = SBDefinitionStatusHistoryRepository(
            FilesystemBridge(SBDefinitionStatusHistoryMapping(), self._base_working_dir)
        )
        self.sbis_status_history = SBInstanceStatusHistoryRepository(
            FilesystemBridge(SBInstanceStatusHistoryMapping(), self._base_working_dir)
        )
        self.ebs_status_history = ExecutionBlockStatusHistoryRepository(
            FilesystemBridge(
                OSOExecutionBlockStatusHistoryMapping(), self._base_working_dir
            )
        )
        self.prjs_status_history = (  # pylint: disable=W0201
            ProjectStatusHistoryRepository(
                FilesystemBridge(ProjectStatusHistoryMapping(), self._base_working_dir)
            )
        )
        return super().__enter__()

    def commit(self) -> None:
        """Implementation of the AbstractUnitOfWork method.

        See :func:`~ska_db_oda.persistence.unitofwork.abstractunitofwork.AbstractUnitOfWork.commit` docstring for details
        """
        self._commit_transactions_for_repo(self.sbds._bridge)
        self._commit_transactions_for_repo(self.sbis._bridge)
        self._commit_transactions_for_repo(self.ebs._bridge)
        self._commit_transactions_for_repo(self.prjs._bridge)
        self._commit_transactions_for_repo(self.prsls._bridge)
        self._commit_transactions_for_repo(self.sbds_status_history._bridge)
        self._commit_transactions_for_repo(self.sbis_status_history._bridge)
        self._commit_transactions_for_repo(self.ebs_status_history._bridge)
        self._commit_transactions_for_repo(self.prjs_status_history._bridge)

    def rollback(self) -> None:
        """Implementation of the AbstractUnitOfWork method.

        See :func:`~ska_db_oda.persistence.unitofwork.abstractunitofwork.AbstractUnitOfWork.rollback` docstring for details
        """
        self._rollback_transactions_for_repo(self.sbds._bridge)
        self._rollback_transactions_for_repo(self.sbis._bridge)
        self._rollback_transactions_for_repo(self.ebs._bridge)
        self._rollback_transactions_for_repo(self.prjs._bridge)
        self._rollback_transactions_for_repo(self.prsls._bridge)
        self._rollback_transactions_for_repo(self.sbds_status_history._bridge)
        self._rollback_transactions_for_repo(self.sbis_status_history._bridge)
        self._rollback_transactions_for_repo(self.ebs_status_history._bridge)
        self._rollback_transactions_for_repo(self.prjs_status_history._bridge)

    def _commit_transactions_for_repo(self, repo_bridge: FilesystemBridge) -> None:
        """
        Mimics the commit of a database transaction by writing the entities
        that are 'pending' in the filesystem Repository
        """
        for path, entity_string in repo_bridge._transactions.items():
            LOGGER.debug("Committing entity to filesystem path %s", path)
            path.write_text(entity_string)
        repo_bridge._transactions.clear()

    def _rollback_transactions_for_repo(self, repo_bridge: FilesystemBridge) -> None:
        """
        Mimics the rollback of a database transaction by removing the entities
        that are 'pending' in the filesystem Repository
        """
        repo_bridge._transactions.clear()
