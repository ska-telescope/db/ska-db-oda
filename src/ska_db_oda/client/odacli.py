# Relax pylint. We deliberately catch all exceptions in the CLI in order to
# return a user-friendly message and rename the ID parameters for each
# entity client class.
#
# pylint: disable=broad-except,arguments-renamed
"""
ODA command-line interface (CLI)

This package contains the CLI used to retrieve and query entities from ODA.
"""

from typing import Generic, List, TypeVar

import fire
from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance

from ska_db_oda.client._strategy import Formatter, get_strategy
from ska_db_oda.persistence.domain.query import QueryParams, QueryParamsFactory

T = TypeVar("T")


class AbstractRepositoryClient(Generic[T]):
    """
    A template class for retrieving data from a repository of
    an entity.

    Extending class should implement private _get and _query methods
    for accessing the given repository.

    :param client: Client should be an instance of ODA's AbstractUnitOfWork
    class.
    """

    def __init__(self, strategy=None):
        if not strategy:
            strategy = get_strategy()
        self._strategy = strategy

    def _get(
        self, entity_id: str, version: int = None, associated_entity: str = None
    ) -> T:
        raise NotImplementedError

    def _query(self, query: QueryParams) -> List[str]:
        raise NotImplementedError

    def get(
        self, entity_id: str, version: int = None, associated_entity: str = None
    ) -> str:
        """
        Retrieve JSON representation of an entity from ODA by ID.
        :param entity_id: entity_id of the entity
        :param version: version of the entity to retrieve data by
                        specific version

        Example usage: oda sbds get sbd-123
        """
        try:
            return Formatter.format_entity_obj(
                self._get(
                    entity_id, version, associated_entity
                )  # pylint: disable=E1121
            )
        except Exception as e:
            return Formatter.format_error(e)

    def query(
        self,
        user=None,
        starts_with=False,
        contains=False,
        created_after=None,
        created_before=None,
        last_modified_after=None,
        last_modified_before=None,
        entity_id=None,
        version=None,
    ) -> dict:
        """
        Retrieve a list of entity IDs given a set of query parameters.

        ODA query functionality currently supports only one type of query
        at a time. The entities can be queried either by user, date created
        or by date modified.

        Example usage:

        oda ebs query --user=user1 --starts_with
        oda sbds query --created_before=20220213
        oda status/history/sbds get entity_id=sbd-mvp01-20240426-5354
        oda status/history/sbds query entity_id=sbd-mvp01-20240426-5354

        :param user: Search for entities created by the given user
        :param starts_with: Used with user and entity_id parameter. If set to True, match
        all entities where the name and entity_id starts with the given name or entity_id
        False by default.
        :params contains: Used with user or entity_id parameter. If set to True,
        return all entities where it will found specified pattern with user or
        entity_id False by default.
        :param created_after: Search for all entities created after the given date.
        Can be combined with created_before to form a date-range.
        :param created_before: Search for all entities created before the given date.
        :param last_modified_after: Search for all entities last modified after the given
        date. Can be combined with last_modified_before to form a date-range.
        :param last_modified_before: Search for all entities last modified before the
        given date.
        :param entity_id: Search for entity_ids created by the given user.
        :param version: version of the entity to retrieve data by
                        specific version
        """
        try:
            match_type = "starts_with" if starts_with else "equals"
            if contains:
                match_type = "contains" if contains else "equals"
            q = QueryParamsFactory.from_dict(
                {
                    "user": user,
                    "match_type": match_type,
                    "created_after": created_after,
                    "created_before": created_before,
                    "last_modified_after": last_modified_after,
                    "last_modified_before": last_modified_before,
                    "entity_id": entity_id,
                    "version": version,
                }
            )
            return Formatter.format_query_response(q, self._query(q))
        except Exception as e:
            return Formatter.format_error(e)


class SBDClient(AbstractRepositoryClient[SBDefinition]):
    def _get(self, sbd_id: str, version: int = None, associated_entity: str = None):
        return self._strategy.get("sbds", sbd_id)

    def _query(self, query: QueryParams) -> List[str]:
        return self._strategy.query("sbds", query)


class SBIClient(AbstractRepositoryClient[SBInstance]):
    def _get(self, sbi_id: str, version: int = None, associated_entity: str = None):
        return self._strategy.get("sbis", sbi_id)

    def _query(self, query) -> List[str]:
        return self._strategy.query("sbis", query)


class EBClient(AbstractRepositoryClient[OSOExecutionBlock]):
    def _get(self, eb_id: str, version: int = None, associated_entity: str = None):
        # TODO BTN-2447
        # if associated_entity:
        #     parent_entity = EBS
        #     eb_obj = self._strategy.entity_relationship.get_relationship(
        #         eb_id, parent_entity, associated_entity
        #     )
        return self._strategy.get("ebs", eb_id)

    def _query(self, query) -> List[str]:
        return self._strategy.query("ebs", query)


class ProjClient(AbstractRepositoryClient[Project]):
    def _get(self, prj_id: str, version: int = None, associated_entity: str = None):
        return self._strategy.get("prjs", prj_id)

    def _query(self, query) -> List[str]:
        return self._strategy.query("prjs", query)


class ProposalClient(AbstractRepositoryClient[Proposal]):
    def _get(self, prsl_id: str, version: int = None, associated_entity: str = None):
        return self._strategy.get("prsls", prsl_id)

    def _query(self, query) -> List[str]:
        return self._strategy.query("prsls", query)


class SBDStatusHistoryClient(AbstractRepositoryClient):
    def _get(
        self, sbd_id: str, version: int = None, associated_entity: str = None
    ):  # pylint: disable=W0221
        with self._strategy:
            sbd_status_history = self._strategy.sbds_status_history.get(
                sbd_id, version, is_status_history=False
            )
        return sbd_status_history

    def _query(self, query) -> List[str]:
        with self._strategy:
            sbd_status_history_ids = self._strategy.sbds_status_history.query(
                query, is_status_history=True
            )
        return sbd_status_history_ids


class SBIStatusHistoryClient(AbstractRepositoryClient):
    def _get(
        self, sbi_id: str, sbi_version: int, associated_entity: str = None
    ):  # pylint: disable=W0221
        with self._strategy:
            sbi_status_history = self._strategy.sbis_status_history.get(
                sbi_id, sbi_version, is_status_history=False
            )
        return sbi_status_history

    def _query(self, query) -> List[str]:
        with self._strategy:
            sbi_status_history_ids = self._strategy.sbis_status_history.query(
                query, is_status_history=True
            )
        return sbi_status_history_ids


class OSOEBStatusHistoryClient(AbstractRepositoryClient):
    def _get(
        self, eb_id: str, eb_version: int, associated_entity: str = None
    ):  # pylint: disable=W0221
        with self._strategy:
            eb_status_history = self._strategy.ebs_status_history.get(
                eb_id, eb_version, is_status_history=False
            )
        return eb_status_history

    def _query(self, query) -> List[str]:
        with self._strategy:
            eb_status_history_ids = self._strategy.ebs_status_history.query(
                query, is_status_history=True
            )
        return eb_status_history_ids


class ProjectStatusHistoryClient(AbstractRepositoryClient):
    def _get(
        self, prj_id: str, version: int = None, associated_entity: str = None
    ):  # pylint: disable=W0221
        with self._strategy:
            prj_status_history = self._strategy.prjs_status_history.get(
                prj_id, version, is_status_history=False
            )
        return prj_status_history

    def _query(self, query) -> List[str]:
        with self._strategy:
            prj_status_history_ids = self._strategy.prjs_status_history.query(
                query, is_status_history=True
            )
        return prj_status_history_ids


class ODAFireUI:
    """
    ODA command-line interface (CLI)

    This CLI is used to retrieve entities from ODA by ID and query
    IDs of entities given a set of query parameters.

    Currently supported entities and commands to access the entities:
    - Scheduling Block Definitions (oda sbds)
    - Scheduling Block Instances (oda sbis)
    - Execution Blocks (oda ebs)
    - Projects (oda prjs)
    - Proposals (oda prsls)
    - status/history/sbds of SBDs (oda sbds status/history/sbds)
    - status/history/sbis of SBIs (oda sbis status/history/sbis)
    - status/history/ebs of EBs (oda ebs status/history/ebs)
    - status/history/prjs of Prjs (oda prjs status/history/prjs)
    """

    sbds = None
    sbis = None
    ebs = None
    prjs = None
    prsls = None
    eb_status_history = None
    sbd_status_history = None
    sbi_status_history = None
    prj_status = None

    def __init__(self):
        """
        Create a new unit of work based client for ODA and initialise
        clients for each entity repository.
        """
        self._strategy = get_strategy()

        self.sbds = SBDClient(self._strategy)
        self.sbis = SBIClient(self._strategy)
        self.ebs = EBClient(self._strategy)
        self.prjs = ProjClient(self._strategy)
        self.prsls = ProposalClient(self._strategy)
        self.eb_status_history = OSOEBStatusHistoryClient(self._strategy)
        self.sbd_status_history = SBDStatusHistoryClient(self._strategy)
        self.sbi_status_history = SBIStatusHistoryClient(self._strategy)
        self.prj_status = ProjectStatusHistoryClient(self._strategy)


def main():
    fire.Fire(ODAFireUI)


if __name__ == "__main__":
    main()
