import json
import os
from http import HTTPStatus
from typing import List, Optional, TypeVar

import requests
from ska_oso_pdm import OSOExecutionBlock, Project, Proposal, SBDefinition, SBInstance

from ska_db_oda.persistence.domain import CODEC, get_identifier
from ska_db_oda.persistence.domain.query import (
    DateQuery,
    QueryParams,
    StatusQuery,
    UserQuery,
)
from ska_db_oda.persistence.unitofwork.filesystemunitofwork import FilesystemUnitOfWork

T = TypeVar("T")


def get_strategy():
    use_filesystem = os.getenv("ODA_CLIENT_USE_FILESYSTEM", "False") == "True"
    if use_filesystem:
        return FilesystemStrategy()
    else:
        return ApiStrategy()


class ApiStrategy:
    def get(self, resource: str, identifier: str):
        resource_url = f"{_get_oda_url()}/{resource}/{identifier}"
        response = requests.get(resource_url)

        if response.status_code == HTTPStatus.OK:
            return _get_type(resource).model_validate_json(response.content)

        if response.status_code == HTTPStatus.NOT_FOUND:
            return KeyError(f"Entity with identifier {identifier} not found.")

        raise ValueError(
            f"Error status {response.status_code} with message {response.content}"
        )

    def query(
        self,
        resource: str,
        qry_params: QueryParams,
    ):
        """ """
        param_string = self._get_param_string(qry_params)
        query_uri = f"{_get_oda_url()}/{resource}{param_string}"
        # TODO BTN-2447
        # if "status" in self._resource_rest_uri:
        #     self._resource_rest_uri = self._resource_rest_uri.replace(
        #         "/status/", "/status/history/"
        #     )
        #
        #     query_uri = self._resource_rest_uri + param_string
        try:
            response = requests.get(query_uri)
        except requests.RequestException as err:
            msg = f"Error retrieving query from {query_uri}: {err.args}"
            raise OSError(msg) from err

        if response.status_code == HTTPStatus.OK:
            # The REST API will return a json list of objects - the TypeAdapter will deserialise this list
            return [
                get_identifier(
                    _get_type(resource).model_validate_json(json.dumps(entity_obj))
                )
                for entity_obj in response.json()
            ]
            # ta = TypeAdapter(List[OSOExecutionBlock])
            # return ta.validate_python(response.json())

        raise OSError(response.content)

    def _get_param_string(
        self, qry_params: QueryParams, is_status_history: Optional[bool] = None
    ):
        if isinstance(qry_params, StatusQuery):
            if is_status_history:
                if qry_params.version:
                    return f"?entity_id={qry_params.entity_id}&version={qry_params.version}"
                return f"?entity_id={qry_params.entity_id}"
            return f"?entity_id={qry_params.entity_id}&match_type={qry_params.match_type.value}"

        if isinstance(qry_params, UserQuery):
            return f"?user={qry_params.user}&match_type={qry_params.match_type.value}"

        if isinstance(qry_params, DateQuery):
            match qry_params.query_type:
                case DateQuery.QueryType.MODIFIED_BETWEEN:
                    field = "last_modified"
                case DateQuery.QueryType.CREATED_BETWEEN:
                    field = "created"
                case _:
                    raise ValueError(f"Unsupported query type {qry_params.query_type}")

            if qry_params.start:
                param_string = f"?{field}_after={qry_params.start.isoformat()}"
                if qry_params.end:
                    param_string += f"&{field}_before={qry_params.end.isoformat()}"
                return param_string
            if qry_params.end:
                return f"?{field}_before={qry_params.end.isoformat()}"

        raise ValueError(
            f"Unsupported query parameters {qry_params.__class__.__name__}"
        )


class FilesystemStrategy:
    def get(self, resource: str, identifier: str):
        with FilesystemUnitOfWork() as uow:
            return getattr(uow, resource).get(identifier)

    def query(self, resource: str, qry_params: QueryParams):
        with FilesystemUnitOfWork() as uow:
            return [
                get_identifier(entity)
                for entity in getattr(uow, resource).query(qry_params)
            ]


def _get_oda_url():
    oda_url = os.getenv("ODA_URL")
    if not oda_url:
        raise KeyError(
            "ODA_URL environment variable is not set. Please set to a running instance"
            " of the ODA, eg"
            " ODA_URL=https://k8s.stfc.skao.int/staging-ska-db-oda/oda/api/v1/"
        )
    # rstrip means it doesn't matter whether the client sets the trailing slash on the env variable
    return oda_url.rstrip("/")


def _get_type(resource):
    match resource:
        case "prsls":
            return Proposal
        case "prjs":
            return Project
        case "sbds":
            return SBDefinition
        case "sbis":
            return SBInstance
        case "ebs":
            return OSOExecutionBlock
    raise TypeError(f"Resource {resource} not supported by ODA.")


class Formatter:
    @staticmethod
    def format_error(e: Exception):
        return str(e)

    @staticmethod
    def format_entity_obj(entity_obj) -> str:
        return json.dumps(json.loads(CODEC.dumps(entity_obj)), indent=4)

    @staticmethod
    def format_query_response(query: QueryParams, item_ids: List[object]):
        query_string = "Query Parameters\n---------------\n"
        qry = {k: v for k, v in query.__dict__.items() if v is not None}

        for attr, value in qry.items():
            query_string = query_string + f"{attr} = {value}\n"
        result_string = "\nResult\n-------\n"
        if not item_ids:
            result_string = result_string + "No matches found"
        else:
            result_string = result_string + json.dumps(item_ids, indent=4)
        return query_string + result_string
